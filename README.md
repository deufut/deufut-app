# DeuFut - App

**Participantes:**

- Daniel Soares Jorge
- Ramon Nobre Peres


**Instruções:**

Primeiramente precisamos configurar o ambiente para desenvolvimento com React Native, o caminho mais simples e rápido para todos os sistemas operacionais é seguindo a documentação desse **[link](https://reactnative.dev/docs/environment-setup)** na aba **"React Native CLI Quickstart"**.

Após seguir o passo a passo para configurar o ambiente, precisamos clonar o projeto através do **[Git](https://git-scm.com/)** por esse **[link](https://gitlab.com/deufut/deufut-app.git)** ou por download direto pelo **[link](https://gitlab.com/deufut/deufut-app/-/archive/master/deufut-app-master.zip)**.


Pelo terminal acesse a pasta do projeto e instale as dependências.

Para instalar via NPM rode o comando:
> _npm install_

Para instalar via YARN rode o comando:
> _yarn_

Para instalar as dependências do iOS rode o comando:

> _npx pod-install ios_

Para dispositivos Android a biblioteca Axios não reconhece o local host (_http://localhost:3333/api ou http://127.0.0.1/3333/api_), vá até o terminal e copie o ipv4 de sua rede, dentro da pasta src/services no arquivo api.js coloque o endereço (_exemplo: http://192.168.0.10:3333/api_).

Pronto! A configuração do projeto está completa e só precisamos rodar o código em um celular via **[USB](https://developer.android.com/studio/debug/dev-options?hl=pt-br)** (precisa ativar a depuração USB) ou pelo emulador através dos comandos:

Para build e rodar em dispositivos Android rodamos o comando:

> _npm react-native run-android_

_OU_

> _yarn react-native run-android_

Para build e rodar em dispositivos iOS rodamos o comando:

> _npm react-native run-ios_

_OU_

> _yarn react-native run-ios_

**O aplicativo será instalado no celular ou emulador e necessitará do servidor rodando (Metro Bundler).**

**Para rodar em dispositivos iOS é necessário estar utilizando o sistema operacional da Apple (macOS).**
