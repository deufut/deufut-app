import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import styled from 'styled-components/native';
import { colors } from '~/styles';

export const Header = styled.View`
  position: relative;
  background: ${colors.primary};
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  height: 70px;
`;

export const Title = styled.Text`
  font-size: 23px;
  font-weight: bold;
  color: ${colors.white};
`;

export const TouchableArea = styled.TouchableOpacity`
  position: absolute;
  left: 17px;
  height: 30px;
  width: 30px;
  justify-content: center;
  align-items: center;
`;

export const IconBack = styled(FontAwesomeIcon)`
  color: ${colors.white};
`;
