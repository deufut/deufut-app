import React from 'react';
import PropTypes from 'prop-types';
import { useNavigation } from '@react-navigation/native';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { Header, Title, IconBack, TouchableArea } from './styles';

export default function HeaderScreen({ title, goBack }) {
  const navigation = useNavigation();

  return (
    <Header>
      {goBack && (
        <TouchableArea onPress={() => navigation.goBack()}>
          <IconBack icon={faArrowLeft} size={22} />
        </TouchableArea>
      )}
      <Title>{title}</Title>
    </Header>
  );
}

HeaderScreen.defaultProps = {
  goBack: false,
};

HeaderScreen.propTypes = {
  goBack: PropTypes.bool,
  title: PropTypes.string.isRequired,
};
