import React from 'react';
import ActivityIndicator from '../ActivityIndicator';
import {
  ModalContainer,
  Alert,
  IconContainer,
  IconStatus,
  Title,
  Subtitle,
  Button,
  TextButton,
  ButtonsContainer,
} from './styles';

export default function Dialog(props) {
  return (
    <ModalContainer
      isVisible={props.visible}
      animationIn="slideInLeft"
      animationOut="slideOutRight"
      onRequestClose={props.close}
      animationInTiming={300}
      onBackdropPress={props.close}
      supportedOrientations={['portrait']}
    >
      <Alert>
        {props.icon && (
          <IconContainer>
            <IconStatus
              icon={props.icon}
              size={props.iconSize}
              color={props.iconColor}
            />
          </IconContainer>
        )}
        {props.title && <Title style={props.titleStyle}>{props.title}</Title>}
        {props.subtitle && (
          <Subtitle style={props.subtitleStyle}>{props.subtitle}</Subtitle>
        )}
        {props.children}
        <ButtonsContainer
          twoButtons={props.leftButtonAction && props.rightButtonAction}
        >
          {props.leftButtonAction && (
            <Button
              onPress={props.leftButtonAction}
              type="leftButton"
              twoButtons={props.leftButtonAction && props.rightButtonAction}
            >
              {!props.leftButtonLoading && (
                <TextButton type="leftButton">
                  {props.leftButtonText}
                </TextButton>
              )}
              {props.leftButtonLoading && (
                <ActivityIndicator margin={0} color="gradientSecondary" />
              )}
            </Button>
          )}
          {props.rightButtonAction && (
            <Button
              onPress={props.rightButtonAction}
              type="rightButton"
              twoButtons={props.leftButtonAction && props.rightButtonAction}
            >
              {!props.rightButtonLoading && (
                <TextButton type="rightButton">
                  {props.rightButtonText}
                </TextButton>
              )}

              {props.rightButtonLoading && (
                <ActivityIndicator margin={0} color="white" />
              )}
            </Button>
          )}
        </ButtonsContainer>
      </Alert>
    </ModalContainer>
  );
}
