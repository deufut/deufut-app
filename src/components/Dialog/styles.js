import styled from 'styled-components/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Modal from 'react-native-modal';
import colors from '~/styles/colors';

export const ModalContainer = styled(Modal)`
  align-items: center;
  justify-content: center;
  margin: 0;
`;

export const Alert = styled.View`
  background-color: ${colors.white};
  align-items: center;
  width: 80%;
  min-width: 300px;
  max-width: 350px;
  padding: 20px;
  padding-top: 30px;
  padding-bottom: 30px;
  justify-content: center;
  align-items: center;
  border-radius: 5px;
`;

export const IconContainer = styled.View`
  width: 80px;
  height: 80px;
  align-items: center;
  justify-content: center;
`;

export const IconStatus = styled(FontAwesomeIcon).attrs((props) => {
  return {
    icon: props.icon,
    size: props.size,
    color: props.color,
  };
})`
  /* color: ${(props) => props.color}; */
`;

export const Title = styled.Text`
  text-align: center;
  font-size: 24px;
  color: ${colors.black};
  font-weight: bold;
  margin-top: 30px;
`;

export const Subtitle = styled.Text`
  text-align: center;
  font-size: 14px;
  color: ${colors.black};
  margin-top: 20px;
`;

export const ButtonsContainer = styled.View`
  width: 100%;
  margin-top: 40px;
  justify-content: ${(props) =>
    props.twoButtons ? 'space-between' : 'center'};
  flex-direction: row;
`;

export const Button = styled.TouchableOpacity`
  border: 1px;
  border-style: solid;
  border-color: ${(props) =>
    props.type === 'leftButton' ? colors.primary : colors.primary};
  background-color: ${(props) =>
    props.type === 'leftButton' ? colors.transparent : colors.primary};
  border-radius: 60px;
  padding-top: 10px;
  padding-bottom: 10px;
  width: ${(props) => (props.twoButtons ? 48 : 90)}%;
  justify-content: center;
  height: 40px;
`;

export const TextButton = styled.Text`
  font-size: 11px;
  font-weight: bold;
  text-align: center;
  color: ${(props) =>
    props.type === 'leftButton' ? colors.primary : colors.white};
`;
