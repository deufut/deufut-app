import React from 'react';

import AwesomeAlert from 'react-native-awesome-alerts';

function Alert({
  title,
  message,
  handleConfirm,
  handleCancel,
  isOpen,
  setIsOpen,
}) {
  return (
    <AwesomeAlert
      show={isOpen}
      showProgress={false}
      title={title}
      message={message}
      closeOnTouchOutside={true}
      closeOnHardwareBackPress={false}
      showCancelButton={true}
      showConfirmButton={true}
      cancelText="CANCELAR"
      confirmText="CONFIRMAR"
      confirmButtonColor="#DD6B55"
      onCancelPressed={() => {
        handleCancel();
        setIsOpen(false);
      }}
      onConfirmPressed={() => {
        handleConfirm();
        setIsOpen(false);
      }}
    />
  );
}

export default Alert;
