import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styled from 'styled-components/native';
import { colors } from '~/styles';

export default KeyboardScrollBackground = styled(KeyboardAwareScrollView).attrs(
  {
    enableOnAndroid: true,
    contentContainerStyle: { flexGrow: 1 },
    showsVerticalScrollIndicator: false,
    showsHorizontalScrollIndicator: false,
    enableAutomaticScroll: true,
    contentInsetAdjustmentBehavior: 'never',
  }
)`
  background: ${colors.white};
`;
