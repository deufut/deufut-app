import styled from 'styled-components/native';
import { colors } from '~/styles';

export default ScrollBackground = styled.ScrollView.attrs({
  showsVerticalScrollIndicator: false,
  showsHorizontalScrollIndicator: false,
  contentInsetAdjustmentBehavior: 'never',
  contentContainerStyle: {
    flexGrow: 1,
  },
})`
  background-color: ${colors.white};
`;
