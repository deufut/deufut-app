import PropTypes from 'prop-types';
import React from 'react';
import { Container, TitleText } from './styles';

function BoxEmpty({ text, children, height }) {
  return (
    <Container height={height}>
      <TitleText>{text}</TitleText>
      {children}
    </Container>
  );
}

BoxEmpty.defaultProps = {
  children: undefined,
  height: 500,
};

BoxEmpty.propTypes = {
  children: PropTypes.element,
  height: PropTypes.number,
  text: PropTypes.string.isRequired,
};

export default BoxEmpty;
