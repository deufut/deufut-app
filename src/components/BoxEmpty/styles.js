import styled from 'styled-components';
import { colors } from '~/styles';

export const Container = styled.View`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: ${(props) => `${props.height}px`};
`;

export const TitleText = styled.Text`
  font-size: 23px;
  color: ${colors.darker};
  font-weight: bold;
`;
