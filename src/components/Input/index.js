import React, { forwardRef } from 'react';
import { TouchableOpacity } from 'react-native';
import { Container, TContainer, TitleInput, TInput, InputIcon } from './styles';

function Input(
  { style, icon, title, height = 47, onPressIcon = null, ...rest },
  ref
) {
  const renderIcon = () => {
    if (!icon) return;

    if (onPressIcon) {
      return (
        <TouchableOpacity underlayColor="none" onPress={onPressIcon}>
          <InputIcon type="solid" icon={icon} />
        </TouchableOpacity>
      );
    }

    return <InputIcon type="solid" icon={icon} />;
  };
  return (
    <Container style={style}>
      {title && <TitleInput>{title}</TitleInput>}
      <TContainer
        height={height}
        background={rest.background ? rest.background : 'inputColor'}
      >
        <TInput
          {...rest}
          ref={ref}
          color={rest.color ? rest.color : 'primary'}
        />
        {renderIcon()}
      </TContainer>
    </Container>
  );
}

export default forwardRef(Input);
