import styled from 'styled-components/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { colors, metrics } from '~/styles';

export const Container = styled.View`
  margin-top: 5px;
  margin-bottom: 5px;
  border-color: ${colors.primary};
  border-width: 1px;
  border-radius: 5px;
`;

export const TContainer = styled.View`
  flex-direction: row;
  align-items: center;
  padding: 0px 20px;
  color: ${colors.primary};
  height: ${(props) => props.height}px;
`;

export const TitleInput = styled.Text`
  font-size: 14px;
  font-weight: bold;
  color: ${colors.fontColor};
  margin-bottom: 13px;
`;

export const TInput = styled.TextInput.attrs((props) => {
  return {
    placeholderTextColor: colors.primary,
  };
})`
  flex: 1;
  height: 100%;
  color: ${colors.primary};
  font-size: 15px;
`;

export const InputIcon = styled(FontAwesomeIcon).attrs((props) => {
  return {
    icon: props.icon,
    color: colors.primary,
    size: metrics.baseIcons,
  };
})``;
