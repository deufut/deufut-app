import React from 'react';
import { Loader } from './styles';

export default function ActivityIndicator({ color = 'buttonColor' }) {
  return <Loader color={color} />;
}
