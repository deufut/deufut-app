import styled from 'styled-components/native';
import { colors } from '~/styles';

export const Loader = styled.ActivityIndicator.attrs((props) => {
  return {
    color: colors[props.color],
    size: 'small',
  };
})`
  margin: 15px 0;
`;
