import styled from 'styled-components/native';
import { colors } from '~/styles';

export default SafeBackground = styled.SafeAreaView`
  flex: 1;
  background: ${colors.primary};
`;
