import axios from 'axios';

export const { CancelToken, isCancel } = axios;

const api = axios.create({
  baseURL: 'http://192.168.15.2:3333/api',
});
export default api;
