import { Platform } from 'react-native';
import Toast from 'react-native-toast-message';

export default function deuFutToast(type, text1, text2) {
  Toast.show({
    type,
    position: 'top',
    text1: type === 'success' ? `${text1}   👍` : `${text1}   👎`,
    text2,
    visibilityTime: 2000,
    autoHide: true,
    topOffset: Platform.OS === 'android' ? 20 : 50,
  });
}
