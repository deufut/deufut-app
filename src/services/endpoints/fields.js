import api from '~/services/api';

export function getFields() {
  function apiCall({ start, length, filters }) {
    return api.post(`/footballfields/get/all`, {
      start,
      length,
      filters,
    });
  }

  return { apiCall };
}

export function getFieldById() {
  function apiCall({ id }) {
    return api.get(`/footballfields/get/${id}`);
  }

  return { apiCall };
}
