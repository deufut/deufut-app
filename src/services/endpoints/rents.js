import api from '~/services/api';

export function getRentById() {
  function apiCall(id) {
    return api.get(`/rents/get/${id}`);
  }

  return { apiCall };
}

export function getRents() {
  function apiCall({ start, length, filters }) {
    return api.post(`/rents/get`, {
      start,
      length,
      filters,
    });
  }

  return { apiCall };
}

export function getRentsFootballField() {
  function apiCall({ footballfield_id, start, length }) {
    return api.post(`/rents/get/footballfield`, {
      footballfield_id,
      start,
      length,
    });
  }

  return { apiCall };
}

export function getRentsTeam() {
  function apiCall({ team_id, start, length }) {
    return api.post(`/rents/get/team`, {
      team_id,
      start,
      length,
    });
  }

  return { apiCall };
}

export function joinRent() {
  function apiCall({ rent_id, team_id }) {
    return api.post(`/rents/join`, {
      rent_id,
      team_id,
    });
  }

  return { apiCall };
}

export function answerRent() {
  function apiCall({ renter_id, answer }) {
    return api.post(`/rents/answer`, {
      renter_id,
      answer,
      team_request: true,
    });
  }

  return { apiCall };
}

export function leaveRent() {
  function apiCall({ renter_id }) {
    return api.delete(`/rents/leave/${renter_id}`);
  }

  return { apiCall };
}
