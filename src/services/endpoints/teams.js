import api from '~/services/api';

export function registerTeam() {
  function apiCall({ name, description, state, city }) {
    return api.post(`/teams/register`, {
      name,
      description,
      state,
      city,
    });
  }

  return { apiCall };
}

export function editTeam() {
  function apiCall({ id, name, description, state, city }) {
    return api.put(`/teams/update`, {
      team_id: id,
      name,
      description,
      state,
      city,
    });
  }

  return { apiCall };
}

export function getMyTeams() {
  function apiCall({ start, length, filters }) {
    return api.post(`/teams/get`, {
      start,
      length,
      filters,
    });
  }

  return { apiCall };
}

export function getAllTeams() {
  function apiCall({ start, length, filters }) {
    return api.post(`/teams/get/all`, {
      start,
      length,
      filters,
    });
  }

  return { apiCall };
}

export function getTeamById() {
  function apiCall({ id }) {
    return api.get(`/teams/get/${id}`);
  }

  return { apiCall };
}

export function leaveTeam() {
  function apiCall({ id }) {
    return api.delete(`/teams/leave/${id}`);
  }

  return { apiCall };
}

export function joinTeam() {
  function apiCall({ id }) {
    return api.post(`/teams/join`, {
      team_id: id,
    });
  }

  return { apiCall };
}

export function kickPlayer() {
  function apiCall({ id }) {
    return api.delete(`/teams/kick/${id}`);
  }

  return { apiCall };
}

export function acceptPlayer() {
  function apiCall({ user_id, team_id }) {
    return api.post(`/teams/answer`, {
      user_id,
      team_id,
      answer: true,
    });
  }

  return { apiCall };
}
