import { AsyncStorage } from 'react-native';
import { persistReducer } from 'redux-persist';

export default (reducers) => {
  const persistedReducer = persistReducer(
    {
      key: 'deufut',
      storage: AsyncStorage,
      whitelist: ['auth', 'users'],
    },
    reducers
  );

  return persistedReducer;
};
