import produce from 'immer';

const INITIAL_STATE = {
  refreshMyTeamList: false,
  refreshAllTeamList: false,
  refreshTeamDetails: false,
  refreshFieldsList: false,
};

export default function utils(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case '@utils/REFRESH_MY_TEAM_LIST': {
        const { refreshMyTeamList } = draft;
        draft.refreshMyTeamList = !refreshMyTeamList;
        break;
      }

      case '@utils/REFRESH_ALL_TEAM_LIST': {
        const { refreshAllTeamList } = draft;
        draft.refreshAllTeamList = !refreshAllTeamList;
        break;
      }

      case '@utils/REFRESH_TEAM_DETAILS': {
        const { refreshTeamDetails } = draft;
        draft.refreshTeamDetails = !refreshTeamDetails;
        break;
      }

      case '@utils/REFRESH_FIELDS_LIST': {
        const { refreshFieldsList } = draft;
        draft.refreshFieldsList = !refreshFieldsList;
        break;
      }
    }
  });
}
