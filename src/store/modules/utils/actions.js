export function refreshMyTeamList() {
  return {
    type: '@utils/REFRESH_MY_TEAM_LIST',
  };
}

export function refreshAllTeamList() {
  return {
    type: '@utils/REFRESH_ALL_TEAM_LIST',
  };
}

export function refreshTeamDetails() {
  return {
    type: '@utils/REFRESH_TEAM_DETAILS',
  };
}

export function refreshFieldsList() {
  return {
    type: '@utils/REFRESH_FIELDS_LIST',
  };
}
