export function addUserInformations(user) {
  return {
    type: '@users/ADD_USER_INFORMATIONS',
    payload: { user },
  };
}

export function removeUserInformations() {
  return {
    type: '@users/REMOVE_USER_INFORMATIONS',
  };
}

export function updateUserAvatarRequest(avatar) {
  return {
    type: '@users/UPDATE_USER_AVATAR_REQUEST',
    payload: { avatar },
  };
}

export function updateUserAvatarSuccess(user) {
  return {
    type: '@users/UPDATE_USER_AVATAR_SUCCESS',
    payload: { user },
  };
}

export function updateUserAvatarFailure() {
  return {
    type: '@users/UPDATE_USER_AVATAR_FAILURE',
  };
}

export function editUserRequest({ name, phone, email }) {
  return {
    type: '@users/EDIT_USER_REQUEST',
    payload: { name, phone, email },
  };
}

export function editUserSuccess(data) {
  return {
    type: '@users/EDIT_USER_SUCCESS',
    payload: { data },
  };
}

export function editUserFailure() {
  return {
    type: '@users/EDIT_USER_FAILURE',
  };
}

export function editPasswordRequest({ password }) {
  return {
    type: '@users/EDIT_PASSWORD_REQUEST',
    payload: { password },
  };
}

export function editPasswordSuccess() {
  return {
    type: '@users/EDIT_PASSWORD_SUCCESS',
  };
}

export function editPasswordFailure() {
  return {
    type: '@users/EDIT_PASSWORD_FAILURE',
  };
}
