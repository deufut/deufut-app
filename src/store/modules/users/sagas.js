import { takeLatest, call, put, all } from 'redux-saga/effects';
import deuFutToast from '~/services/toast';
import api, { CancelToken } from '~/services/api';
import {
  updateUserAvatarSuccess,
  updateUserAvatarFailure,
  editUserFailure,
  editUserSuccess,
  editPasswordSuccess,
  editPasswordFailure,
} from './actions';

let signal;

export function* updateAvatar() {
  const { avatar } = action.payload;
  signal = CancelToken.source();

  try {
    const response = yield call(
      api.patch,
      '/users/avatar',
      {
        avatar,
      },
      { cancelToken: signal.token }
    );
    yield put(updateUserAvatarSuccess(response.data));
    deuFutToast(
      'success',
      'Avatar atualizado!',
      'Agora é só curtir o novo visual.'
    );
  } catch (err) {
    yield put(updateUserAvatarFailure());
    deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
  }
}

export function* editUserInfo({ payload }) {
  const { name, email, phone } = payload;

  try {
    signal = CancelToken.source();
    const response = yield call(
      api.put,
      `/users/update`,
      {
        name,
        phone,
        email,
      },
      { cancelToken: signal.token }
    );

    yield put(editUserSuccess(response.data));
    deuFutToast(
      'success',
      'Perfil atualizado!',
      'Suas informações agora estão em dia.'
    );
  } catch (err) {
    yield put(editUserFailure());
    deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
  }
}

export function* editPasswordInfo({ payload }) {
  const { password } = payload;

  try {
    signal = CancelToken.source();
    yield call(
      api.put,
      `/users/update`,
      {
        password,
      },
      { cancelToken: signal.token }
    );

    yield put(editPasswordSuccess());
    deuFutToast(
      'success',
      'Senha atualizada!',
      'Sua conta mais segura do que nunca.'
    );
  } catch (err) {
    yield put(editPasswordFailure());
    deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
  }
}

export function cancelSignal() {
  signal.cancel();
}

export default all([
  takeLatest('@users/UPDATE_USER_AVATAR_REQUEST', updateAvatar),
  takeLatest('@users/EDIT_USER_REQUEST', editUserInfo),
  takeLatest('@users/EDIT_PASSWORD_REQUEST', editPasswordInfo),
]);
