import produce from 'immer';

const INITIAL_STATE = {
  user: null,
  loading: false,
};

export default function users(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case '@users/ADD_USER_INFORMATIONS': {
        draft.user = action.payload.user;
        break;
      }

      case '@users/REMOVE_USER_INFORMATIONS': {
        draft.user = null;
        break;
      }

      case '@users/UPDATE_USER_AVATAR_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@users/UPDATE_USER_AVATAR_SUCCESS': {
        draft.user = action.payload.user;
        draft.loading = false;
        break;
      }

      case '@users/UPDATE_USER_AVATAR_FAILURE': {
        draft.loading = false;
        break;
      }

      case '@users/EDIT_USER_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@users/EDIT_USER_SUCCESS': {
        draft.user = action.payload.data;
        draft.loading = false;
        break;
      }

      case '@users/EDIT_USER_FAILURE': {
        draft.loading = false;
        break;
      }

      case '@users/EDIT_PASSWORD_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@users/EDIT_PASSWORD_SUCCESS': {
        draft.loading = false;
        break;
      }

      case '@users/EDIT_PASSWORD_FAILURE': {
        draft.loading = false;
        break;
      }
    }
  });
}
