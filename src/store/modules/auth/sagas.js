import { takeLatest, call, put, all, select } from 'redux-saga/effects';
import deuFutToast from '~/services/toast';
import api, { CancelToken } from '~/services/api';
import {
  signInSuccess,
  signInFailure,
  signUpSuccess,
  signUpFailure,
} from './actions';
import { addUserInformations, removeUserInformations } from '../users/actions';

let signal;

export function* signIn({ payload }) {
  const { email, password } = payload;
  signal = CancelToken.source();

  try {
    const response = yield call(
      api.post,
      '/sessions',
      {
        email,
        password,
      },
      { cancelToken: signal.token }
    );

    const { token, user } = response.data;

    api.defaults.headers.Authorization = `Bearer ${token}`;

    yield put(addUserInformations(user));
    yield put(signInSuccess(token));
  } catch (err) {
    yield put(signInFailure());
    deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
  }
}

export function cancelSignal() {
  signal.cancel();
}

export function* signUp({ payload }) {
  try {
    const { name, email, password, phone } = payload;
    signal = CancelToken.source();

    const response = yield call(
      api.post,
      '/users/register',
      {
        name,
        email,
        password,
        phone,
      },
      { cancelToken: signal.token }
    );
    yield put(signUpSuccess(response.data));
    deuFutToast('success', 'Conta criada!', 'Agora é entrar na plataforma.');
  } catch (err) {
    yield put(signUpFailure());
    deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
  }
}

export function* signOut() {
  yield put(removeUserInformations());
  delete api.defaults.headers.Authorization;
}

// export function* forgotPassword({ payload }) {
//   const { email } = payload;
//   signal = CancelToken.source();

//   try {
//     const response = yield call(
//       api.post,
//       '/users/forgotpassword',
//       {
//         email,
//       },
//       { cancelToken: signal.token }
//     );
//     yield put(forgotPasswordSuccess());
//   } catch (err) {
//     yield put(forgotPasswordFailure());
// }

export function setToken({ payload }) {
  if (!payload) return;

  const { token } = payload.auth;

  if (token) {
    api.defaults.headers.Authorization = `Bearer ${token}`;
  }
}

export default all([
  takeLatest('persist/REHYDRATE', setToken),
  takeLatest('@auth/SIGN_IN_REQUEST', signIn),
  takeLatest('@auth/SIGN_UP_REQUEST', signUp),
  takeLatest('@auth/SIGN_OUT', signOut),
  // takeLatest('@auth/FORGOT_PASSWORD_REQUEST', forgotPassword),
]);
