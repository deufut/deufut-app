import { combineReducers } from 'redux';

import auth from './auth/reducer';
import users from './users/reducer';
import utils from './utils/reducer';

export default combineReducers({
  auth,
  users,

  utils,
});
