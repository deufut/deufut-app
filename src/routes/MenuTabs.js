import React from 'react';
import { View, StyleSheet } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import LinearGradient from 'react-native-linear-gradient';
import Teams from '~/assets/svg/teams.svg';
import Fields from '~/assets/svg/fields.svg';
import Profile from '~/assets/svg/profile.svg';
import SearchField from '~/assets/svg/searchFields.svg';
import Ball from '~/assets/svg/ball.svg';
import colors from '~/styles/colors';

import {
  FieldStackNavigator,
  ProfileStackNavigator,
  MyTeamStackNavigator,
  AllTeamsStackNavigator,
  SeekDepartureStackNavigator,
} from './AppStack';

const Tab = createBottomTabNavigator();

const MenuTabs = () => (
  <Tab.Navigator
    screenOptions={({ route }) => ({
      tabBarIcon: ({ color }) => {
        if (route.name === 'Campos') {
          return <Fields width={28} height={28} fill={color} />;
        }
        if (route.name === 'Perfil') {
          return <Profile width={25} height={25} fill={color} />;
        }
        if (route.name === 'Meus times') {
          return <Teams width={25} height={25} fill={color} />;
        }
        if (route.name === 'Buscar times') {
          return <SearchField width={22} height={22} fill={color} />;
        }
      },
    })}
    tabBarOptions={{
      activeTintColor: colors.primary,
      inactiveTintColor: colors.black,
      showLabel: false,
    }}
    initialRouteName="Buscar partidas"
  >
    <Tab.Screen
      name="Buscar times"
      component={AllTeamsStackNavigator}
      options={{
        unmountOnBlur: true,
      }}
    />
    <Tab.Screen
      name="Campos"
      component={FieldStackNavigator}
      options={{
        unmountOnBlur: true,
      }}
    />
    <Tab.Screen
      name="Buscar partidas"
      component={SeekDepartureStackNavigator}
      options={() => ({
        unmountOnBlur: true,
        tabBarIcon: () => (
          <View>
            <LinearGradient
              style={styles.iconTabRound}
              start={{ x: 0, y: 1 }}
              end={{ x: 0, y: 0 }}
              colors={['#2D9B12', '#4e5f1c']}
            >
              <Ball width={26} height={26} fill="#FFF" />
            </LinearGradient>
          </View>
        ),
      })}
    />
    <Tab.Screen
      name="Meus times"
      component={MyTeamStackNavigator}
      options={{
        unmountOnBlur: true,
      }}
    />
    <Tab.Screen
      name="Perfil"
      component={ProfileStackNavigator}
      options={{
        unmountOnBlur: true,
      }}
    />
  </Tab.Navigator>
);

export default MenuTabs;

const styles = StyleSheet.create({
  iconTabRound: {
    width: 60,
    height: 60,
    borderRadius: 30,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 6,
    shadowColor: '#556b2f',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
  },
});
