import React from 'react';
import { useSelector } from 'react-redux';
import MenuTabs from './MenuTabs';
import AuthRoutes from './auth.routes';

function Routes() {
  const signed = useSelector((state) => state.auth.signed);
  return signed ? <MenuTabs /> : <AuthRoutes />;
}

export default Routes;
