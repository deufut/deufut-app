import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import SignUp from '~/screen/SignUp';
import SignIn from '~/screen/SignIn';

function AuthRoutes() {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="LogIn" component={SignIn} />
      <Stack.Screen name="SignUp" component={SignUp} />
    </Stack.Navigator>
  );
}

export default AuthRoutes;
