import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Field from '~/screen/Field';
import MyTeams from '~/screen/MyTeams';
import Teams from '~/screen/Teams';
import Profile from '~/screen/Profile';
import FieldDetails from '~/screen/FieldDetails';
import RegisterTeam from '~/screen/RegisterTeam';
import EditPassword from '~/screen/EditPassword';
import TeamDetails from '~/screen/DetailsTeam';
import SeekDeparture from '~/screen/SeekDeparture';
import DepartureDetails from '~/screen/DepartureDetails';
import MyTeamDetails from '~/screen/MyDetailsTeam';
import EditTeam from '~/screen/EditTeam';
import JoinRent from '~/screen/JoinRent';
import TeamRents from '~/screen/TeamRents';
import DepartureDetailsTeam from '~/screen/DepartureDetailsTeam';

const FieldStack = createStackNavigator();
const FieldStackNavigator = () => (
  <FieldStack.Navigator headerMode="none">
    <FieldStack.Screen name="Campos" component={Field} />
    <FieldStack.Screen name="DetalhesCampo" component={FieldDetails} />
    <FieldStack.Screen name="EfetuarReserva" component={JoinRent} />
  </FieldStack.Navigator>
);

const ProfileStack = createStackNavigator();
const ProfileStackNavigator = () => (
  <ProfileStack.Navigator headerMode="none">
    <ProfileStack.Screen name="Perfil" component={Profile} />
    <ProfileStack.Screen name="AlterarSenha" component={EditPassword} />
  </ProfileStack.Navigator>
);

const MyTeamStack = createStackNavigator();
const MyTeamStackNavigator = () => (
  <MyTeamStack.Navigator headerMode="none">
    <MyTeamStack.Screen name="MeusTimes" component={MyTeams} />
    <MyTeamStack.Screen name="CadastroTimes" component={RegisterTeam} />
    <MyTeamStack.Screen name="DetalhesMeusTimes" component={MyTeamDetails} />
    <MyTeamStack.Screen name="EditarTime" component={EditTeam} />
    <MyTeamStack.Screen name="AlugueisTime" component={TeamRents} />
    <MyTeamStack.Screen
      name="DetalhesPartidaTime"
      component={DepartureDetailsTeam}
    />
  </MyTeamStack.Navigator>
);

const AllTeamsStack = createStackNavigator();
const AllTeamsStackNavigator = () => (
  <AllTeamsStack.Navigator headerMode="none">
    <AllTeamsStack.Screen name="Times" component={Teams} />
    <AllTeamsStack.Screen name="DetalhesTimes" component={TeamDetails} />
  </AllTeamsStack.Navigator>
);

const SeekDepartureStack = createStackNavigator();
const SeekDepartureStackNavigator = () => (
  <SeekDepartureStack.Navigator headerMode="none">
    <SeekDepartureStack.Screen name="BuscarPartida" component={SeekDeparture} />
    <SeekDepartureStack.Screen
      name="DetalhesPartida"
      component={DepartureDetails}
    />
    <SeekDepartureStack.Screen name="EfetuarReserva" component={JoinRent} />
  </SeekDepartureStack.Navigator>
);

export {
  FieldStackNavigator,
  ProfileStackNavigator,
  MyTeamStackNavigator,
  AllTeamsStackNavigator,
  SeekDepartureStackNavigator,
};
