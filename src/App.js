import 'react-native-gesture-handler';
import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Toast from 'react-native-toast-message';
import Orientation from 'react-native-orientation-locker';
import Routes from './routes';

const App = () => {
  useEffect(() => {
    Orientation.lockToPortrait();
    console.disableYellowBox = true;
  }, []);

  return (
    <NavigationContainer>
      <Routes />
      <Toast ref={(ref) => Toast.setRef(ref)} />
    </NavigationContainer>
  );
};

export default App;
