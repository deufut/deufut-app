export default {
  white: '#FFF',
  light: '#DDD',
  lighter: '#F5F5F5',
  regular: '#C3CACF',
  dark: '#666',
  darker: '#333',
  black: '#1a1a1a',

  primary: '#2D9B12',
  secondary: '#A6B1C3',
  success: '#2DAA39',
  warning: '#FFAA2B',
  danger: '#e37a7a',
  info: '#1067AA',
  reverse: '#123954',

  danger2: '#F32525',

  ice: '#f0f2f7',
  inputColor: '#E8EBF0',
  buttonColor: '#4057E3',
  fontColor: '#1F2D50',
  gradientPrimary: '#1269B0',
  gradientSecondary: '#00A99D',

  transparent: 'transparent',
  darkTransparent: 'rgba(0,0,0,0.6)',
  whiteTransparent: 'rgba(255,255,255,0.3)',
};
