import styled from 'styled-components';
import { colors } from '~/styles';

export const Container = styled.View`
  flex: 1;
  margin-left: 0;
  margin-right: 0;
  background: ${colors.white};
`;

export const Button = styled.TouchableOpacity`
  width: 250px;
  height: 40px;
  margin-top: 5px;
  border-radius: 5px;
  background: ${(props) => (props.color ? colors.primary : colors.dark)};
  align-self: center;
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text`
  color: ${colors.white};
  font-size: 15px;
  font-weight: bold;
`;

export const WrapperTeams = styled.ScrollView`
  padding: 10px;
  padding-top: 15px;
  padding-bottom: 15px;
`;

export const BoxButton = styled.View`
  height: 80px;
  background-color: white;
  width: 100%;
`;

export const Wrapper = styled.View``;

export const Title = styled.Text`
  font-size: 22px;
  font-weight: bold;
  color: ${colors.fontColor};
  margin-bottom: 5px;
  margin-top: 10px;
  margin-left: 10px;
`;

export const BoxTeams = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 15px;
  margin-bottom: 10px;
  border-width: 1px;
  border-color: grey;
  border-radius: 10px;
`;

export const Box = styled.View`
  border: 1px solid ${colors.darker};
  margin: 10px;
  border-radius: 10px;
  height: ${(props) => props.height}px;
`;
