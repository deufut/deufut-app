import React, { useEffect, useState } from 'react';
import { Text, Dimensions, RefreshControl } from 'react-native';
import { useRoute, useNavigation } from '@react-navigation/native';
import { RadioButton } from 'react-native-paper';
import HeaderScreen from '~/components/HeaderScreens';
import SafeBackground from '~/components/SafeBackground';
import BoxLoading from '~/components/BoxLoading';
import BoxEmpty from '~/components/BoxEmpty';
import { getMyTeams } from '~/services/endpoints/teams';
import { joinRent } from '~/services/endpoints/rents';
import deuFutToast from '~/services/toast';
import {
  Container,
  Button,
  ButtonText,
  WrapperTeams,
  BoxButton,
  Wrapper,
  Title,
  BoxTeams,
  Box,
} from './styles';

const { height } = Dimensions.get('screen');

function JoinRent() {
  const route = useRoute();
  const navigation = useNavigation();
  const { rent_id, update } = route.params;
  const [checked, setChecked] = useState();
  const [indexChecked, setIndexChecked] = useState();
  const [recordsTotal, setRecordsTotal] = useState(0);
  const [teams, setTeams] = useState([]);
  const [loading, setLoading] = useState(true);
  const [loadingRequest, setLoadingRequest] = useState(false);

  useEffect(() => {
    loadMyTeams();
  }, []);

  async function loadMyTeams() {
    const { apiCall } = getMyTeams();
    try {
      const response = await apiCall({
        start: 0,
        length: 0,
        filters: { owner: true },
      });
      const { data, records } = response.data;
      setRecordsTotal(records);
      setTeams(data);
      setLoading(false);
    } catch (err) {
      navigation.goBack();
      deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
      setLoading(false);
    }
  }

  async function handleJoinRent() {
    const { apiCall } = joinRent();
    setLoadingRequest(true);
    try {
      await apiCall({
        rent_id,
        team_id: checked,
      });
      setLoadingRequest(false);
      deuFutToast(
        'success',
        'Pedido enviado!',
        'Agora é só aguardar a resposta da solicitação.'
      );
      if (update !== undefined) {
        update();
      }
      navigation.goBack();
    } catch (err) {
      deuFutToast(
        'error',
        'Algo deu errado!',
        err.response.data.message || 'Tente novamente mais tarde.'
      );
      setLoadingRequest(false);
    }
  }

  return (
    <SafeBackground>
      <Container>
        <HeaderScreen title="Efetuar reserva" goBack />
        {loading && <BoxLoading />}
        {!loading && teams.length > 0 ? (
          <>
            <Wrapper>
              <Title>Meus times</Title>
              <Box height={height - 350}>
                <WrapperTeams
                  refreshControl={
                    <RefreshControl
                      tintColor="transarent"
                      onRefresh={() => loadMyTeams()}
                      refreshing={false}
                    />
                  }
                >
                  {teams.map((team, index) => (
                    <BoxTeams key={team.id} onPress={() => setChecked(team.id)}>
                      <RadioButton
                        value={team.id}
                        status={checked === team.id ? 'checked' : 'unchecked'}
                        onPress={() => {
                          setChecked(team.id);
                          setIndexChecked(index);
                        }}
                        color="#2D9B12"
                      />
                      <Text
                        style={{
                          fontSize: 15,
                          color: '#1F2D50',
                          fontWeight: 'bold',
                          marginLeft: 5,
                        }}
                      >
                        {team.name}
                      </Text>
                    </BoxTeams>
                  ))}
                </WrapperTeams>
              </Box>
            </Wrapper>
            <BoxButton>
              <Button
                color={checked}
                onPress={() => {
                  if (checked !== undefined) {
                    handleJoinRent();
                  }
                }}
              >
                <ButtonText>SOLICITAR</ButtonText>
              </Button>
            </BoxButton>
          </>
        ) : (
          <BoxEmpty text="Nenhum time encontrado!" />
        )}
      </Container>
    </SafeBackground>
  );
}

export default JoinRent;
