import styled from 'styled-components/native';
import { colors } from '~/styles';
import logo from '~/assets/images/logoDeuFut.png';

export const Content = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  background: ${colors.white};
`;

export const Form = styled.View`
  margin-top: 20px;
  margin-bottom: 20px;
`;

export const Button = styled.TouchableOpacity`
  width: 300px;
  height: 40px;
  border-radius: 5px;
  margin-top: 20px;
  background: ${colors.primary};
  align-self: center;
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text`
  color: ${colors.white};
  font-size: 15px;
  font-weight: bold;
`;

export const Splash = styled.View`
  height: 200px;
  margin-bottom: 20px;
  justify-content: center;
  align-items: center;
`;

export const Logo = styled.Image.attrs({
  source: logo,
})`
  height: 200px;
  width: 200px;
`;

export const ForgotPasswordContainer = styled.TouchableOpacity`
  flex-direction: row;
  justify-content: flex-end;
`;

export const RegisterContainer = styled.TouchableOpacity`
  flex-direction: row;
  justify-content: flex-end;
  margin-top: 20px;
`;

export const LinkText = styled.Text`
  color: ${colors.primary};
  font-size: 15px;
  text-decoration: underline;
`;
