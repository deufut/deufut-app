import React, { useState, useEffect } from 'react';
import {
  faUser,
  faLock,
  faEye,
  faEyeSlash,
} from '@fortawesome/free-solid-svg-icons';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { signInRequest, setLoading } from '~/store/modules/auth/actions';
import KeyboardScrollBackground from '~/components/KeyboardScrollBackground';
import Input from '~/components/Input';
import ActivityIndicator from '~/components/ActivityIndicator';
import {
  Content,
  Form,
  Button,
  ButtonText,
  Splash,
  Logo,
  RegisterContainer,
  ForgotPasswordContainer,
  LinkText,
} from './styles';
import { colors } from '~/styles';

export default function SignIn() {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const { loading } = useSelector((state) => state.auth);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(true);

  useEffect(() => {
    dispatch(setLoading(false));
  }, []);

  function handleSubmit() {
    dispatch(signInRequest(email, password));
  }

  const renderPassIcon = () => {
    if (password === '') return faLock;
    if (showPassword) return faEye;
    return faEyeSlash;
  };

  return (
    <KeyboardScrollBackground backgroundColor={colors.ice}>
      <Content>
        <Form>
          <Splash>
            <Logo />
          </Splash>
          <Input
            keyboardType="email-address"
            autoCorrect={false}
            autoCapitalize="none"
            value={email}
            onChangeText={(text) => setEmail(text)}
            placeholder="E-mail"
            onSubmitEditing={() => passwordInput.focus()}
            blurOnSubmit={false}
            icon={faUser}
          />
          <Input
            ref={(input) => (passwordInput = input)}
            autoCorrect={false}
            autoCapitalize="none"
            value={password}
            onPressIcon={
              password.length > 0 ? () => setShowPassword(!showPassword) : null
            }
            onChangeText={(text) => setPassword(text)}
            placeholder="Senha"
            secureTextEntry={showPassword}
            icon={renderPassIcon()}
            onSubmitEditing={() => (!loading ? handleSubmit() : null)}
          />
          {/* <ForgotPasswordContainer>
            <LinkText>Esqueceu sua senha?</LinkText>
          </ForgotPasswordContainer> */}
          <Button onPress={() => (!loading ? handleSubmit() : null)}>
            {loading ? (
              <ActivityIndicator color="white" />
            ) : (
              <ButtonText>ENTRAR</ButtonText>
            )}
          </Button>
          <RegisterContainer onPress={() => navigation.navigate('SignUp')}>
            <LinkText>Não possui cadastro?</LinkText>
          </RegisterContainer>
        </Form>
      </Content>
    </KeyboardScrollBackground>
  );
}
