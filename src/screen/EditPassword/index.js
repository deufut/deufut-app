import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import SafeBackground from '~/components/SafeBackground';
import deuFutToast from '~/services/toast';
import {
  Container,
  Button,
  ButtonText,
  Wrapper,
  BoxImage,
  BoxInput,
  Input,
  CategoryProfile,
  Form,
} from './styles';
import HeaderScreen from '~/components/HeaderScreens';
import ActivityIndicator from '~/components/ActivityIndicator';
import ImageKey from '~/assets/svg/key.svg';
import { editPasswordRequest } from '~/store/modules/users/actions';

export default function EditPassword() {
  const dispatch = useDispatch();
  const { loading } = useSelector((state) => state.users);
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');

  useEffect(() => {
    return () => {
      setPassword('');
      setPasswordConfirmation('');
    };
  }, []);

  function handleSubmit() {
    if (!password || !passwordConfirmation) {
      deuFutToast('error', 'Algo deu errado!', 'Algum campo está vazio.');
      return;
    }
    if (password === passwordConfirmation) {
      dispatch(
        editPasswordRequest({
          password,
        })
      );
    } else {
      deuFutToast('error', 'Algo deu errado!', 'Senhas não coincidem.');
    }
  }

  return (
    <SafeBackground>
      <Container>
        <HeaderScreen title="Editar senha" goBack />
        <Wrapper>
          <BoxImage>
            <ImageKey width={150} height={150} fill="#333" />
          </BoxImage>
          <Form>
            <BoxInput>
              <CategoryProfile>Nova senha</CategoryProfile>
              <Input
                autoCorrect={false}
                autoCapitalize="none"
                value={password}
                onChangeText={(text) => setPassword(text)}
                placeholder="Senha"
                secureTextEntry
                blurOnSubmit={false}
                onSubmitEditing={() => passwordConfirmationInput.focus()}
              />

              <CategoryProfile>Confirmação de senha</CategoryProfile>
              <Input
                ref={(input) => (passwordConfirmationInput = input)}
                autoCorrect={false}
                autoCapitalize="none"
                value={passwordConfirmation}
                onChangeText={(text) => setPasswordConfirmation(text)}
                placeholder="Repetir a senha"
                secureTextEntry
                onSubmitEditing={() => handleSubmit()}
              />
            </BoxInput>
            <Button
              onPress={() => {
                if (!loading) {
                  handleSubmit();
                }
              }}
            >
              {loading ? (
                <ActivityIndicator />
              ) : (
                <ButtonText>ALTERAR SENHA</ButtonText>
              )}
            </Button>
          </Form>
        </Wrapper>
      </Container>
    </SafeBackground>
  );
}
