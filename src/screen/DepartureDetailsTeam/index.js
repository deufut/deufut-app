import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import {
  faCrown,
  faTimesCircle,
  faCheckCircle,
} from '@fortawesome/free-solid-svg-icons';
import { useNavigation, useRoute } from '@react-navigation/native';
import { getRentById, answerRent, leaveRent } from '~/services/endpoints/rents';
import deuFutToast from '~/services/toast';
import Dialog from '~/components/Dialog';
import SafeBackground from '~/components/SafeBackground';
import HeaderScreen from '~/components/HeaderScreens';
import BoxLoading from '~/components/BoxLoading';
import {
  Container,
  Wrapper,
  WrapperDetails,
  BoxDetails,
  CategoryText,
  DetailsText,
  RolePlayer,
} from './styles';

export default function DepartureDetailsTeam() {
  const route = useRoute();
  const navigation = useNavigation();
  const { rent_id, team_id, player_role, removeRent } = route.params;
  const [rent, setRent] = useState();
  const [loading, setLoading] = useState(true);
  const [manipulation, setManipulation] = useState(false);
  const [renterID, setRenterID] = useState();
  const [teamIndex, setTeamIndex] = useState();
  const [teamName, setTeamName] = useState('');
  const [showLeaveModal, setShowLeaveModal] = useState(false);
  const [showKickModal, setShowKickModal] = useState(false);

  useEffect(() => {
    getRent();
  }, []);

  useEffect(() => {
    if (rent) {
      const index = rent.teams.findIndex((team) => team.id === team_id);
      if (index >= 0) {
        if (rent.teams[index].role === 'Dono' && player_role === 'Dono') {
          setManipulation(true);
        } else {
          setManipulation(false);
        }
      } else {
        setManipulation(false);
      }
    }
  }, [rent]);

  const getRent = () => {
    setTimeout(async () => {
      const { apiCall } = getRentById();
      try {
        const response = await apiCall(rent_id);
        setRent(response.data);
        setLoading(false);
      } catch (err) {
        setLoading(false);
        deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
        navigation.goBack();
      }
    }, 1000);
  };

  const answerRentRequest = async (renter_id, answer, index) => {
    const { apiCall } = answerRent();
    try {
      await apiCall({ renter_id, answer });
      deuFutToast(
        'success',
        'Tudo certo!',
        'Resposta do pedido foi salva com sucesso.'
      );
      const rentAux = { ...rent };
      if (answer) {
        rentAux.teams[index].role = 'Participante';
      } else {
        delete rentAux.teams[index];
      }
      setRent(rentAux);
    } catch (err) {
      console.log(err.response.data);
      deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
    }
  };

  const leaveTeamRent = async () => {
    const index = rent.teams.findIndex((team) => team.id === team_id);
    if (index < 0) {
      return;
    }

    const { apiCall } = leaveRent();
    try {
      await apiCall({ renter_id: rent.teams[index].renter_id });
      deuFutToast(
        'success',
        'Tudo certo!',
        'Time não participará mais dessa partida.'
      );
      removeRent(rent.id);
      navigation.goBack();
    } catch (err) {
      deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
    }
  };

  return (
    <SafeBackground>
      <Container>
        <HeaderScreen title="Detalhes da partida" goBack />
        {loading && <BoxLoading />}
        {!loading && rent && (
          <Wrapper contentContainerStyle={{ paddingBottom: 40 }}>
            <WrapperDetails>
              <BoxDetails>
                <CategoryText>Campo: </CategoryText>
                <DetailsText numberOfLines={1}>
                  {rent.footballfield.name}
                </DetailsText>
              </BoxDetails>
              <BoxDetails>
                <CategoryText>Endereço: </CategoryText>
                <DetailsText>{rent.footballfield.address}</DetailsText>
              </BoxDetails>
              <BoxDetails>
                <CategoryText>Número: </CategoryText>
                <DetailsText>{rent.footballfield.number}</DetailsText>
              </BoxDetails>
              <BoxDetails>
                <CategoryText>Bairro: </CategoryText>
                <DetailsText>{rent.footballfield.neighborhood}</DetailsText>
              </BoxDetails>
              <BoxDetails>
                <CategoryText>Cidade: </CategoryText>
                <DetailsText>{rent.footballfield.city}</DetailsText>
              </BoxDetails>
              <BoxDetails>
                <CategoryText>Estado: </CategoryText>
                <DetailsText>{rent.footballfield.state}</DetailsText>
              </BoxDetails>
              <BoxDetails>
                <CategoryText>Data: </CategoryText>
                <DetailsText numberOfLines={1}>
                  {new Date(rent.date_start).getUTCDate()}/
                  {new Date(rent.date_start).getUTCMonth() + 1}/
                  {new Date(rent.date_start).getUTCFullYear()}
                </DetailsText>
              </BoxDetails>
              <BoxDetails>
                <CategoryText>Início: </CategoryText>
                <DetailsText numberOfLines={1}>
                  {new Date(rent.date_start).getHours()}:
                  {String(new Date(rent.date_start).getMinutes()).padStart(
                    2,
                    '0'
                  )}
                </DetailsText>
              </BoxDetails>
              <BoxDetails>
                <CategoryText>Fim: </CategoryText>
                <DetailsText numberOfLines={1}>
                  {new Date(rent.date_end).getHours()}:
                  {String(new Date(rent.date_end).getMinutes()).padStart(
                    2,
                    '0'
                  )}
                </DetailsText>
              </BoxDetails>
              <BoxDetails>
                <CategoryText>Valor: </CategoryText>
                <DetailsText numberOfLines={1}>
                  R$ {String(rent.value.toFixed(2)).replace('.', ',')}
                </DetailsText>
              </BoxDetails>
              {player_role === 'Dono' && (
                <TouchableOpacity
                  style={{
                    position: 'absolute',
                    alignItems: 'center',
                    justifyContent: 'center',
                    top: 10,
                    right: 10,
                  }}
                  onPress={() => setShowLeaveModal(true)}
                >
                  <FontAwesomeIcon
                    icon={faTimesCircle}
                    size={28}
                    color="#F32525"
                  />
                </TouchableOpacity>
              )}
            </WrapperDetails>
            <View
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
                flexDirection: 'row',
                marginTop: 10,
                marginBottom: 10,
              }}
            >
              <View
                style={{
                  height: 2,
                  width: '30%',
                  backgroundColor: '#A6B1C3',
                }}
              />
              <Text style={{ color: '#A6B1C3' }}>Times participantes</Text>
              <View
                style={{
                  height: 2,
                  width: '30%',
                  backgroundColor: '#A6B1C3',
                }}
              />
            </View>
            {rent.teams.map((team, index) => (
              <WrapperDetails key={team.id}>
                <BoxDetails
                  key={team.id}
                  style={{ justifyContent: 'space-between' }}
                >
                  <View style={{ flexDirection: 'row' }}>
                    <CategoryText>Time: </CategoryText>
                    <DetailsText numberOfLines={1}>{team.name}</DetailsText>
                  </View>
                  {team.role === 'Dono' && (
                    <FontAwesomeIcon icon={faCrown} size={28} color="#FFAA2B" />
                  )}

                  {team.role !== 'Dono' &&
                    team.role === 'Pendente' &&
                    manipulation && (
                      <View
                        style={{
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: 'center',
                          flexDirection: 'row',
                        }}
                      >
                        <TouchableOpacity
                          onPress={() => {
                            answerRentRequest(team.renter_id, true, index);
                          }}
                        >
                          <FontAwesomeIcon
                            style={{ marginRight: 5 }}
                            icon={faCheckCircle}
                            size={28}
                            color="#2D9B12"
                          />
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={() => {
                            setTeamName(team.name);
                            setRenterID(team.renter_id);
                            setTeamIndex(index);
                            setShowKickModal(true);
                          }}
                        >
                          <FontAwesomeIcon
                            icon={faTimesCircle}
                            size={28}
                            color="#F32525"
                          />
                        </TouchableOpacity>
                      </View>
                    )}

                  {team.role !== 'Dono' &&
                    team.role === 'Participante' &&
                    manipulation && (
                      <View
                        style={{
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: 'center',
                          flexDirection: 'row',
                        }}
                      >
                        <TouchableOpacity
                          onPress={() => {
                            setTeamName(team.name);
                            setRenterID(team.renter_id);
                            setTeamIndex(index);
                            setShowKickModal(true);
                          }}
                        >
                          <FontAwesomeIcon
                            icon={faTimesCircle}
                            size={28}
                            color="#F32525"
                          />
                        </TouchableOpacity>
                      </View>
                    )}

                  {team.role !== 'Dono' && !manipulation && (
                    <RolePlayer rolePlayer={team.role}>{team.role}</RolePlayer>
                  )}
                </BoxDetails>
              </WrapperDetails>
            ))}
          </Wrapper>
        )}
        <Dialog
          visible={showLeaveModal}
          close={() => setShowLeaveModal(false)}
          icon={faTimesCircle}
          iconSize={80}
          iconColor="#F32525"
          title="Deseja sair do aluguél?"
          subtitle="Essa ação não poderá ser desfeita."
          leftButtonAction={() => setShowLeaveModal(false)}
          rightButtonAction={() => {
            leaveTeamRent();
            setShowLeaveModal(false);
          }}
          leftButtonText="CANCELAR"
          rightButtonText="CONFIRMAR"
        />
        <Dialog
          visible={showKickModal}
          close={() => setShowKickModal(false)}
          icon={faTimesCircle}
          iconSize={80}
          iconColor="#F32525"
          title={`Deseja expulsar o ${teamName} do aluguél?`}
          subtitle="Essa ação não poderá ser desfeita."
          leftButtonAction={() => setShowKickModal(false)}
          rightButtonAction={() => {
            answerRentRequest(renterID, false, teamIndex);
            setShowKickModal(false);
          }}
          leftButtonText="CANCELAR"
          rightButtonText="CONFIRMAR"
        />
      </Container>
    </SafeBackground>
  );
}
