import styled from 'styled-components/native';
import { colors } from '~/styles';

export const Container = styled.View`
  flex: 1;
  margin-left: 0;
  margin-right: 0;
  background: ${colors.white};
`;

export const Wrapper = styled.ScrollView`
  padding: 10px;
`;

export const WrapperDetails = styled.View`
  padding: 5px;
  margin: 10px;
  border-width: 1px;
  border-color: ${colors.black};
  border-radius: 10px;
`;

export const BoxDetails = styled.View`
  display: flex;
  align-items: center;
  flex-direction: row;
  padding: 5px;
  overflow: hidden;
  margin-right: 10px;
`;

export const CategoryText = styled.Text`
  font-size: 20px;
  font-weight: bold;
  color: #a6b1c3;
`;

export const DetailsText = styled.Text`
  font-size: 18px;
`;

export const RolePlayer = styled.Text`
  font-size: 15px;
  font-style: italic;
  font-weight: bold;
  color: ${(props) =>
    props.rolePlayer === 'Participante' ? colors.gradientPrimary : '#a9a9a9'};
`;
