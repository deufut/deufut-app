import styled from 'styled-components/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { colors } from '~/styles';

export const Container = styled.View`
  flex: 1;
  margin-left: 0;
  margin-right: 0;
  background: ${colors.white};
`;

export const WrapperTeams = styled.ScrollView`
  padding: 10px;
  padding-top: 15px;
`;

export const BoxTeam = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: 18px 15px;
  margin-bottom: 15px;
  border-width: 1px;
  border-color: grey;
  border-radius: 10px;
`;

export const TeamName = styled.Text`
  font-size: 22px;
  color: ${colors.black};
`;

export const DetailsTeam = styled(FontAwesomeIcon)`
  color: ${colors.primary};
`;

export const SubHeader = styled.View`
  height: 13%;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  padding-bottom: 10px;
  border-bottom-width: 1px;
  border-bottom-color: grey;
  padding: 10px;
`;

export const InputWrapper = styled.View`
  display: flex;
  flex-direction: row;
  background: #fff;
  border-radius: 8px;
  border: 1px solid #dcdce6;
  width: 100%;
  align-items: center;
  color: #666369;
`;

export const Input = styled.TextInput`
  flex: 1;
  background: transparent;
  border: 0;
  color: #666360;
  padding: 15px;
`;

export const IconSearch = styled(FontAwesomeIcon)`
  color: #006400;
  margin-right: 15px;
  margin-left: 15px;
`;

export const BoxRight = styled.View``;

export const RolePlayer = styled.Text`
  color: ${colors.secondary};
  margin-top: 3px;
`;
