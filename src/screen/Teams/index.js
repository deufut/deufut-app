import React, { useEffect, useState } from 'react';
import { RefreshControl, TouchableOpacity } from 'react-native';
import { faArrowRight, faSearch } from '@fortawesome/free-solid-svg-icons';
import { useNavigation } from '@react-navigation/native';
import { useSelector } from 'react-redux';
import SafeBackground from '~/components/SafeBackground';
import HeaderScreen from '~/components/HeaderScreens';
import deuFutToast from '~/services/toast';
import {
  Container,
  WrapperTeams,
  DetailsTeam,
  TeamName,
  BoxTeam,
  SubHeader,
  BoxRight,
  InputWrapper,
  Input,
  IconSearch,
} from './styles';

import { getAllTeams } from '~/services/endpoints/teams';
import BoxLoading from '~/components/BoxLoading';
import BoxEmpty from '~/components/BoxEmpty';

export default function Teams() {
  const navigation = useNavigation();
  const { refreshAllTeamList } = useSelector((state) => state.utils);
  const [name, setName] = useState('');
  const [teams, setTeams] = useState([]);
  const [loading, setLoading] = useState(true);
  const [recordsTotal, setRecordsTotal] = useState(0);

  async function loadAllTeams() {
    const { apiCall } = getAllTeams();
    try {
      const response = await apiCall({
        start: 0,
        length: 0,
        filters: {
          name,
          state: '',
          city: '',
        },
      });
      const { data, records } = response.data;
      setRecordsTotal(records);
      setTeams(data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
    }
  }

  useEffect(() => {
    loadAllTeams();
  }, [refreshAllTeamList]);

  return (
    <SafeBackground>
      <Container>
        <HeaderScreen title="Buscar times" />
        <SubHeader>
          <InputWrapper>
            <Input
              placeholder="Buscar nome do time"
              ref={(input) => (nameInput = input)}
              autoCorrect={false}
              value={name}
              onChangeText={(text) => setName(text)}
              onSubmitEditing={() => loadAllTeams()}
            />
            <TouchableOpacity onPress={() => loadAllTeams()}>
              <IconSearch size={22} icon={faSearch} />
            </TouchableOpacity>
          </InputWrapper>
        </SubHeader>
        {loading && <BoxLoading />}
        {!loading && (
          <WrapperTeams
            refreshControl={
              <RefreshControl
                tintColor="transarent"
                onRefresh={() => loadAllTeams()}
                refreshing={false}
              />
            }
            contentContainerStyle={{ paddingBottom: 20 }}
          >
            {!!teams.length && (
              <>
                {teams.map((team) => (
                  <BoxTeam
                    key={team.id}
                    onPress={() =>
                      navigation.navigate('DetalhesTimes', { id: team.id })
                    }
                  >
                    <BoxRight>
                      <TeamName>{team.name}</TeamName>
                    </BoxRight>
                    <DetailsTeam icon={faArrowRight} size={20} />
                  </BoxTeam>
                ))}
              </>
            )}
            {recordsTotal === 0 && <BoxEmpty text="Nenhum time encontrado!" />}
          </WrapperTeams>
        )}
      </Container>
    </SafeBackground>
  );
}
