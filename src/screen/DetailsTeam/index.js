import React, { useCallback, useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import {
  faCrown,
  faUserPlus,
  faSpinner,
} from '@fortawesome/free-solid-svg-icons';
import { View, Text } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import SafeBackground from '~/components/SafeBackground';
import {
  Container,
  Wrapper,
  WrapperDetails,
  BoxDetailsTeam,
  CategoryText,
  DetailsText,
  ButtonOutTeam,
  WrapperPlayers,
  BoxPlayer,
  PlayerName,
} from './styles';
import HeaderScreen from '~/components/HeaderScreens';
import deuFutToast from '~/services/toast';
import BoxLoading from '~/components/BoxLoading';
import { getTeamById, joinTeam } from '~/services/endpoints/teams';
import { colors } from '~/styles';
import { refreshAllTeamList } from '~/store/modules/utils/actions';

export default function TeamDetails() {
  const route = useRoute();
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(false);
  const [loadingRequest, setLoadingRequest] = useState(false);
  const [data, setData] = useState([]);
  const [players, setPlayers] = useState([]);

  const loadTeam = useCallback(async () => {
    const { apiCall } = getTeamById();
    setLoading(true);
    try {
      const response = await apiCall({ id: route.params.id });
      setData(response.data);
      setPlayers(response.data.players);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
    }
  }, []);

  useEffect(() => {
    loadTeam();
  }, [loadTeam]);

  async function handleJoinTeam() {
    const { apiCall } = joinTeam();

    setLoadingRequest(true);
    try {
      await apiCall({ id: route.params.id });
      setLoadingRequest(false);
      deuFutToast(
        'success',
        'Solicitação enviada!',
        'Aguarde o dono do time responder sua solicitação.'
      );
      dispatch(refreshAllTeamList());
      navigation.navigate('Times');
    } catch (err) {
      setLoadingRequest(false);
      deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
    }
  }

  return (
    <SafeBackground>
      <Container>
        <HeaderScreen title="Detalhes do time" goBack />
        {loading && <BoxLoading />}
        {!loading && (
          <>
            <WrapperDetails>
              <BoxDetailsTeam>
                <CategoryText>Nome: </CategoryText>
                <DetailsText numberOfLines={1}>{data.name}</DetailsText>
              </BoxDetailsTeam>
              <BoxDetailsTeam>
                <CategoryText>Estado: </CategoryText>
                <DetailsText>{data.state}</DetailsText>
              </BoxDetailsTeam>
              <BoxDetailsTeam>
                <CategoryText>Cidade: </CategoryText>
                <DetailsText>{data.city}</DetailsText>
              </BoxDetailsTeam>
              <BoxDetailsTeam>
                <CategoryText>Descrição: </CategoryText>
                <DetailsText>{data.description}</DetailsText>
              </BoxDetailsTeam>
              <ButtonOutTeam onPress={handleJoinTeam}>
                {loadingRequest ? (
                  <FontAwesomeIcon
                    icon={faSpinner}
                    size={28}
                    spin
                    color={colors.buttonColor}
                  />
                ) : (
                  <FontAwesomeIcon
                    icon={faUserPlus}
                    size={28}
                    color={colors.buttonColor}
                  />
                )}
              </ButtonOutTeam>
            </WrapperDetails>
            <View
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
                flexDirection: 'row',
                marginTop: 10,
                marginBottom: 10,
              }}
            >
              <View
                style={{
                  height: 2,
                  width: '32%',
                  backgroundColor: '#A6B1C3',
                }}
              />
              <Text style={{ color: '#A6B1C3' }}>Jogadores do time</Text>
              <View
                style={{
                  height: 2,
                  width: '32%',
                  backgroundColor: '#A6B1C3',
                }}
              />
            </View>
            <Wrapper>
              <WrapperPlayers>
                <>
                  {players.map((player) => (
                    <BoxPlayer key={player.id}>
                      <PlayerName>{player.name}</PlayerName>
                      {player.role === 'Dono' && (
                        <FontAwesomeIcon
                          icon={faCrown}
                          size={28}
                          color="#FFAA2B"
                        />
                      )}
                    </BoxPlayer>
                  ))}
                </>
              </WrapperPlayers>
            </Wrapper>
          </>
        )}
      </Container>
    </SafeBackground>
  );
}
