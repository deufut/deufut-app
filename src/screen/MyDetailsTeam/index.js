import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, RefreshControl } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import {
  faCalendarAlt,
  faCheckCircle,
  faCrown,
  faEdit,
  faSpinner,
  faTimesCircle,
} from '@fortawesome/free-solid-svg-icons';
import { useNavigation, useRoute } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { refreshMyTeamList } from '~/store/modules/utils/actions';
import {
  getTeamById,
  leaveTeam,
  kickPlayer,
  acceptPlayer,
} from '~/services/endpoints/teams';
import Dialog from '~/components/Dialog';
import deuFutToast from '~/services/toast';
import BoxLoading from '~/components/BoxLoading';
import SafeBackground from '~/components/SafeBackground';
import HeaderScreen from '~/components/HeaderScreens';
import {
  Container,
  Wrapper,
  WrapperDetails,
  BoxDetailsTeam,
  CategoryText,
  DetailsText,
  ButtonOutTeam,
  WrapperPlayers,
  BoxPlayer,
  PlayerName,
  DetailsTextDescription,
  BoxButtons,
  IconEdit,
  RolePlayer,
} from './styles';

export default function MyTeamDetails() {
  const dispatch = useDispatch();
  const route = useRoute();
  const navigation = useNavigation();
  const { refreshTeamDetails } = useSelector((state) => state.utils);
  const { id } = useSelector((state) => state.users.user);
  const [showLeaveModal, setShowLeaveModal] = useState(false);
  const [showKickModal, setShowKickModal] = useState(false);
  const [loading, setLoading] = useState(true);
  const [loadingDelete, setLoadingDelete] = useState(false);
  const [data, setData] = useState([]);
  const [players, setPlayers] = useState([]);
  const [rolePlayer, setRolePlayer] = useState('');
  const [playerName, setPlayerName] = useState('');
  const [playerID, setPlayerID] = useState();

  const loadTeam = useCallback(async () => {
    const { apiCall } = getTeamById();
    try {
      const response = await apiCall({ id: route.params.id });
      response.data.players.forEach((player) => {
        if (player.id === id) {
          setRolePlayer(player.role);
        }
      });
      setLoading(false);
      setData(response.data);
      setPlayers(response.data.players);
    } catch (err) {
      setLoading(false);
      deuFutToast(
        'error',
        'Falha',
        'Erro ao carregar dados do time, tente novamente mais tarde...'
      );
    }
  }, []);

  useEffect(() => {
    loadTeam();
  }, [loadTeam, refreshTeamDetails]);

  async function handleLeaveTeam() {
    const { apiCall } = leaveTeam();
    setLoadingDelete(true);
    try {
      await apiCall({ id: route.params.id });
      setLoadingDelete(false);
      deuFutToast(
        'success',
        'Sucesso',
        `Você saiu do time ${data.name} com sucesso`
      );
      dispatch(refreshMyTeamList());
      navigation.navigate('MeusTimes');
    } catch (err) {
      setLoadingDelete(false);
      deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
    }
  }

  const handleKickPlayer = useCallback(async (playerId) => {
    const { apiCall } = kickPlayer();
    try {
      await apiCall({ id: playerId });

      loadTeam();
    } catch (err) {
      deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
    }
  }, []);

  const handleAcceptPlayer = useCallback(async (userId) => {
    const { apiCall } = acceptPlayer();
    try {
      await apiCall({ user_id: userId, team_id: route.params.id });
      loadTeam();
    } catch (err) {
      deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
    }
  }, []);

  return (
    <SafeBackground>
      <Container>
        <HeaderScreen title="Detalhes do time" goBack />
        {loading && <BoxLoading />}
        {!loading && (
          <>
            <WrapperDetails>
              <BoxDetailsTeam>
                <CategoryText>Nome: </CategoryText>
                <DetailsText numberOfLines={1}>{data.name}</DetailsText>
              </BoxDetailsTeam>
              <BoxDetailsTeam>
                <CategoryText>Estado: </CategoryText>
                <DetailsText>{data.state}</DetailsText>
              </BoxDetailsTeam>
              <BoxDetailsTeam>
                <CategoryText>Cidade: </CategoryText>
                <DetailsText>{data.city}</DetailsText>
              </BoxDetailsTeam>
              <BoxDetailsTeam>
                <CategoryText>Descrição: </CategoryText>
                <DetailsTextDescription numberOfLines={5}>
                  {data.description}
                </DetailsTextDescription>
              </BoxDetailsTeam>
              <BoxButtons>
                {rolePlayer !== 'Pendente' && (
                  <ButtonOutTeam
                    onPress={() =>
                      navigation.navigate('AlugueisTime', {
                        team_id: data.id,
                        player_role: rolePlayer,
                      })
                    }
                  >
                    <IconEdit icon={faCalendarAlt} size={28} color="#1269B0" />
                  </ButtonOutTeam>
                )}
                {rolePlayer === 'Dono' && (
                  <>
                    <ButtonOutTeam
                      onPress={() =>
                        navigation.navigate('EditarTime', {
                          id: route.params.id,
                        })
                      }
                    >
                      <IconEdit icon={faEdit} size={28} color="#1269B0" />
                    </ButtonOutTeam>
                  </>
                )}
                <ButtonOutTeam onPress={() => setShowLeaveModal(true)}>
                  {!loadingDelete ? (
                    <FontAwesomeIcon
                      icon={faTimesCircle}
                      size={28}
                      color="#F32525"
                    />
                  ) : (
                    <FontAwesomeIcon
                      icon={faSpinner}
                      size={28}
                      spin
                      color="#F32525"
                    />
                  )}
                </ButtonOutTeam>
              </BoxButtons>
            </WrapperDetails>
            <View
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
                flexDirection: 'row',
                marginTop: 10,
                marginBottom: 10,
              }}
            >
              <View
                style={{
                  height: 2,
                  width: '32%',
                  backgroundColor: '#A6B1C3',
                }}
              />
              <Text style={{ color: '#A6B1C3' }}>Jogadores do time</Text>
              <View
                style={{
                  height: 2,
                  width: '32%',
                  backgroundColor: '#A6B1C3',
                }}
              />
            </View>
            <Wrapper
              refreshControl={
                <RefreshControl
                  tintColor="transarent"
                  onRefresh={() => loadTeam()}
                  refreshing={false}
                />
              }
            >
              <WrapperPlayers>
                <>
                  {players.map((player) => (
                    <BoxPlayer key={player.id}>
                      <PlayerName rolePlayer={player.role}>
                        {player.name}
                      </PlayerName>
                      {player.role === 'Dono' && (
                        <FontAwesomeIcon
                          icon={faCrown}
                          size={28}
                          color="#FFAA2B"
                        />
                      )}
                      {rolePlayer !== 'Dono' && player.role === 'Pendente' && (
                        <RolePlayer rolePlayer={player.role}>
                          Pendente
                        </RolePlayer>
                      )}
                      {rolePlayer !== 'Dono' && player.role === 'Membro' && (
                        <RolePlayer rolePlayer={player.role}>
                          Jogador
                        </RolePlayer>
                      )}
                      {rolePlayer === 'Dono' && player.role === 'Pendente' && (
                        <View
                          style={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                            flexDirection: 'row',
                          }}
                        >
                          <TouchableOpacity
                            style={{
                              height: 30,
                              width: 30,
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}
                            onPress={() => handleAcceptPlayer(player.id)}
                          >
                            <FontAwesomeIcon
                              style={{ marginRight: 5 }}
                              icon={faCheckCircle}
                              size={28}
                              color="#2D9B12"
                            />
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={{
                              height: 30,
                              width: 30,
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}
                            onPress={() => {
                              setPlayerID(player.player_id);
                              setPlayerName(player.name);
                              setShowKickModal(true);
                            }}
                          >
                            <FontAwesomeIcon
                              icon={faTimesCircle}
                              size={28}
                              color="#F32525"
                            />
                          </TouchableOpacity>
                        </View>
                      )}
                      {rolePlayer === 'Dono' && player.role === 'Membro' && (
                        <TouchableOpacity
                          style={{
                            height: 30,
                            width: 30,
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}
                          onPress={() => {
                            setPlayerID(player.player_id);
                            setPlayerName(player.name);
                            setShowKickModal(true);
                          }}
                        >
                          <FontAwesomeIcon
                            icon={faTimesCircle}
                            size={28}
                            color="#F32525"
                          />
                        </TouchableOpacity>
                      )}
                    </BoxPlayer>
                  ))}
                </>
              </WrapperPlayers>
            </Wrapper>
          </>
        )}
        <Dialog
          visible={showKickModal}
          close={() => setShowKickModal(false)}
          icon={faTimesCircle}
          iconSize={80}
          iconColor="#F32525"
          title={`Deseja expulsar o ${playerName} do time?`}
          subtitle="Essa ação não poderá ser desfeita."
          leftButtonAction={() => setShowKickModal(false)}
          rightButtonAction={() => {
            handleKickPlayer(playerID);
            setShowKickModal(false);
          }}
          leftButtonText="CANCELAR"
          rightButtonText="CONFIRMAR"
        />
        <Dialog
          visible={showLeaveModal}
          close={() => setShowLeaveModal(false)}
          icon={faTimesCircle}
          iconSize={80}
          iconColor="#F32525"
          title="Deseja sair do time?"
          subtitle="Essa ação não poderá ser desfeita."
          leftButtonAction={() => setShowLeaveModal(false)}
          rightButtonAction={() => {
            handleLeaveTeam();
            setShowLeaveModal(false);
          }}
          leftButtonText="CANCELAR"
          rightButtonText="CONFIRMAR"
        />
      </Container>
    </SafeBackground>
  );
}
