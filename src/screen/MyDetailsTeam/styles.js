import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import styled from 'styled-components/native';
import { colors } from '~/styles';

export const Container = styled.View`
  flex: 1;
  margin-left: 0;
  margin-right: 0;
  background: ${colors.white};
`;

export const Title = styled.Text`
  font-size: 22px;
  font-weight: bold;
  color: ${colors.fontColor};
  margin-top: 13px;
  margin-left: 20px;
  margin-bottom: 5px;
`;

export const Wrapper = styled.ScrollView`
  padding: 10px;
`;

export const WrapperDetails = styled.View`
  position: relative;
  padding: 5px;
  margin: 10px;
  border-width: 1px;
  border-color: ${colors.black};
  border-radius: 10px;
`;

export const BoxDetailsTeam = styled.View`
  display: flex;
  align-items: center;
  flex-direction: row;
  padding: 5px;
  overflow: hidden;
  margin-right: 110px;
`;

export const CategoryText = styled.Text`
  font-size: 20px;
  font-weight: bold;
  color: #a6b1c3;
`;

export const DetailsText = styled.Text`
  font-size: 18px;
`;

export const DetailsTextDescription = styled.Text`
  font-size: 18px;
`;

export const ButtonOutTeam = styled.TouchableOpacity``;

export const WrapperPlayers = styled.View``;

export const BoxPlayer = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: 15px 15px;
  margin-bottom: 15px;
  border-width: 1px;
  border-color: grey;
  border-radius: 10px;
`;

export const PlayerName = styled.Text`
  font-size: 20px;
  font-weight: bold;
  color: ${(props) =>
    props.rolePlayer === 'Pendente' ? '#a9a9a9' : colors.black};
`;

export const BoxButtons = styled.View`
  display: flex;
  align-items: center;
  flex-direction: row;
  position: absolute;
  top: 10px;
  right: 10px;
`;

export const IconEdit = styled(FontAwesomeIcon)`
  margin-right: 5px;
`;

export const RolePlayer = styled.Text`
  font-size: 15px;
  font-style: italic;
  font-weight: bold;
  color: ${(props) =>
    props.rolePlayer === 'Membro' ? colors.gradientPrimary : '#a9a9a9'};
`;
