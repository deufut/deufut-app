import styled from 'styled-components/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { colors } from '~/styles';
import IconPlus from '~/assets/svg/iconPlus.svg';

export const Container = styled.View`
  flex: 1;
  margin-left: 0;
  margin-right: 0;
  background: ${colors.white};
`;

export const WrapperTeams = styled.ScrollView`
  padding: 10px;
`;

export const BoxTeam = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: 7px 15px;
  margin-bottom: 15px;
  border-width: 1px;
  border-color: grey;
  border-radius: 10px;
`;

export const TeamName = styled.Text`
  font-size: 22px;
  color: ${colors.black};
`;

export const DetailsTeam = styled(FontAwesomeIcon)`
  color: ${colors.primary};
`;

export const SubHeader = styled.TouchableOpacity`
  height: 13%;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  padding-bottom: 10px;
  border-bottom-width: 1px;
  border-bottom-color: grey;
`;

export const TextRegisterTeam = styled.Text`
  font-size: 30px;
  font-weight: bold;
  color: #333;
`;

export const ButtonPlus = styled(IconPlus)`
  margin-left: 10px;
`;

export const BoxRight = styled.View``;

export const RolePlayer = styled.Text`
  color: ${colors.secondary};
  margin-top: 3px;
`;

export const Button = styled.TouchableOpacity`
  width: 250px;
  height: 40px;
  border-radius: 5px;
  margin-top: 20px;
  background: ${colors.primary};
  align-self: center;
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text`
  color: ${colors.white};
  font-size: 15px;
  font-weight: bold;
`;
