import React, { useEffect, useState } from 'react';
import { RefreshControl } from 'react-native';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import { useNavigation } from '@react-navigation/native';
import { useSelector } from 'react-redux';
import { getMyTeams } from '~/services/endpoints/teams';
import deuFutToast from '~/services/toast';
import SafeBackground from '~/components/SafeBackground';
import HeaderScreen from '~/components/HeaderScreens';
import BoxLoading from '~/components/BoxLoading';
import BoxEmpty from '~/components/BoxEmpty';
import {
  Container,
  WrapperTeams,
  DetailsTeam,
  TeamName,
  BoxTeam,
  SubHeader,
  TextRegisterTeam,
  ButtonPlus,
  BoxRight,
  RolePlayer,
  Button,
  ButtonText,
} from './styles';

export default function MyTeams() {
  const navigation = useNavigation();
  const [teams, setTeams] = useState([]);
  const [loading, setLoading] = useState(true);
  const [recordsTotal, setRecordsTotal] = useState(0);

  const { refreshMyTeamList } = useSelector((state) => state.utils);

  async function loadMyTeams() {
    const { apiCall } = getMyTeams();
    try {
      const response = await apiCall({ start: 0, length: 0 });
      const { data, records } = response.data;
      setRecordsTotal(records);
      setTeams(data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
    }
  }

  useEffect(() => {
    loadMyTeams();
  }, [refreshMyTeamList]);

  return (
    <SafeBackground>
      <Container>
        <HeaderScreen title="Meus times" />
        <SubHeader onPress={() => navigation.navigate('CadastroTimes')}>
          <TextRegisterTeam>Cadastre seu time</TextRegisterTeam>
          <ButtonPlus width={30} height={30} fill="#2D9B12" />
        </SubHeader>
        {loading && <BoxLoading />}
        {!loading && !!teams.length && (
          <WrapperTeams
            refreshControl={
              <RefreshControl
                tintColor="transarent"
                onRefresh={() => loadMyTeams()}
                refreshing={false}
              />
            }
            contentContainerStyle={{ paddingBottom: 20 }}
          >
            {teams.map((team) => (
              <BoxTeam
                key={team.id}
                onPress={() =>
                  navigation.navigate('DetalhesMeusTimes', { id: team.id })
                }
              >
                <BoxRight>
                  <TeamName>{team.name}</TeamName>
                  <RolePlayer>
                    {team.role === 'Membro' ? 'Jogador' : team.role}
                  </RolePlayer>
                </BoxRight>
                <DetailsTeam icon={faArrowRight} size={20} />
              </BoxTeam>
            ))}
          </WrapperTeams>
        )}
        {!loading && recordsTotal === 0 && (
          <BoxEmpty text="Nenhum time cadastrado!">
            <Button onPress={() => navigation.navigate('CadastroTimes')}>
              <ButtonText>CADASTRAR TIME</ButtonText>
            </Button>
          </BoxEmpty>
        )}
      </Container>
    </SafeBackground>
  );
}
