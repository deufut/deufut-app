import styled from 'styled-components/native';
import { colors } from '~/styles';

export const Container = styled.View`
  flex: 1;
  margin-left: 0;
  margin-right: 0;
  background: ${colors.white};
`;

export const Box = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const Title = styled.Text`
  font-size: 30px;
  font-weight: bold;
  color: ${colors.primary};
  margin-top: 15px;
  align-self: center;
`;

export const Button = styled.TouchableOpacity`
  width: 250px;
  height: 40px;
  border-radius: 5px;
  margin-top: 20px;
  background: ${colors.primary};
  align-self: center;
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text`
  color: ${colors.white};
  font-size: 15px;
  font-weight: bold;
`;

export const BallContainer = styled.View`
  margin: auto;
`;

export const BallTouchableOpacity = styled.TouchableOpacity`
  height: 210px;
  width: 210px;
  border-radius: 500px;
`;

export const BallBox = styled.View`
  height: 210px;
  width: 210px;
  border-radius: 500px;
  background: ${colors.primary};
  justify-content: center;
  align-items: center;
`;

export const WrapperRents = styled.ScrollView`
  padding: 10px;
`;

export const BoxRent = styled.TouchableOpacity`
  display: flex;
  margin-bottom: 15px;
  border-width: 1px;
  border-color: grey;
  border-radius: 10px;
`;

export const BoldText = styled.Text.attrs({ numberOfLines: 1 })`
  color: ${colors.black};
  font-size: 18px;
  font-weight: bold;
`;

export const NormalText = styled.Text`
  font-weight: normal;
`;

export const RowContainer = styled.View`
  flex-direction: row;
`;

export const BoxContainer = styled.View`
  padding: 7.5px 15px;
`;

export const Line = styled.View`
  height: 1px;
  background: ${colors.black};
`;
