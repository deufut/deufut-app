import React, { useState } from 'react';
import { Animated, Easing } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { getRents } from '~/services/endpoints/rents';
import SafeBackground from '~/components/SafeBackground';
import HeaderScreen from '~/components/HeaderScreens';
import BoxEmpty from '~/components/BoxEmpty';
import deuFutToast from '~/services/toast';
import {
  Container,
  Box,
  BallTouchableOpacity,
  BallContainer,
  BallBox,
  Title,
  Button,
  ButtonText,
  BoxRent,
  WrapperRents,
  BoldText,
  NormalText,
  RowContainer,
  BoxContainer,
  Line,
} from './styles';
import Ball from '~/assets/svg/ball.svg';

export default function SeekDeparture() {
  const navigation = useNavigation();
  const [searchable, setSearchable] = useState(true);
  const [rents, setRents] = useState([]);
  const [total, setTotal] = useState(0);
  const [request, setRequest] = useState(true);
  const [spinValue] = useState(new Animated.Value(0));

  const animation = Animated.loop(
    Animated.timing(spinValue, {
      toValue: 1,
      duration: 2000,
      easing: Easing.linear,
      useNativeDriver: true,
    })
  );

  const spin = spinValue.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg'],
  });

  const handleSubmit = () => {
    setTimeout(async () => {
      const { apiCall } = getRents();
      try {
        const response = await apiCall({
          start: 0,
          length: 0,
          filters: {
            requestable: true,
            reserved: true,
          },
        });
        setRents(response.data.data);
        setTotal(response.data.records);
        setRequest(false);
      } catch (err) {
        setRequest(false);
        deuFutToast(
          'error',
          'Algo deu errado!',
          err.response.data.message || 'Tente novamente mais tarde.'
        );
      }
    }, 4000);
  };

  const BeforeRequest = () => {
    return (
      <BallContainer>
        <BallTouchableOpacity
          onPress={() => {
            if (searchable) {
              animation.start();
              setSearchable(false);
              handleSubmit();
            }
          }}
        >
          <BallBox as={Animated.View} style={{ transform: [{ rotate: spin }] }}>
            <Ball width={200} height={200} fill="#FFF" />
          </BallBox>
        </BallTouchableOpacity>
        <Title>Vem pro fut!</Title>
      </BallContainer>
    );
  };

  const AfterRequest = () => {
    if (rents.length > 0) {
      return (
        <>
          <WrapperRents>
            {rents.map((rent) => {
              const dateStart = new Date(rent.date_start);
              const dateEnd = new Date(rent.date_end);
              return (
                <BoxRent
                  key={rent.id}
                  onPress={() =>
                    navigation.navigate('DetalhesPartida', {
                      rent_id: rent.id,
                      request: true,
                    })
                  }
                >
                  <BoxContainer>
                    <BoldText style={{ fontSize: 25 }}>
                      {rent.footballfield.name}
                    </BoldText>
                  </BoxContainer>
                  <Line />
                  <RowContainer>
                    <BoxContainer>
                      <BoldText style={{ marginBottom: 15 }}>
                        Data:{' '}
                        <NormalText>
                          {dateStart.getUTCDate()}/{dateStart.getUTCMonth() + 1}
                          /{dateStart.getUTCFullYear()}
                        </NormalText>
                      </BoldText>
                      <BoldText>
                        Valor:{' '}
                        <NormalText>
                          R$ {String(rent.value.toFixed(2)).replace('.', ',')}
                        </NormalText>
                      </BoldText>
                    </BoxContainer>
                    <BoxContainer>
                      <BoldText style={{ marginBottom: 15 }}>
                        Início:{' '}
                        <NormalText>
                          {dateStart.getHours()}:
                          {String(dateStart.getMinutes()).padStart(2, '0')}
                        </NormalText>
                      </BoldText>
                      <BoldText>
                        Término:{' '}
                        <NormalText>
                          {dateEnd.getHours()}:
                          {String(dateEnd.getMinutes()).padStart(2, '0')}
                        </NormalText>
                      </BoldText>
                    </BoxContainer>
                  </RowContainer>
                  <BoxContainer>
                    <BoldText>
                      Time: <NormalText>{rent.team?.name}</NormalText>
                    </BoldText>
                  </BoxContainer>
                </BoxRent>
              );
            })}
          </WrapperRents>
        </>
      );
    }

    return (
      <Box>
        <BoxEmpty text="Nenhuma partida encontrada!">
          <Button
            onPress={() => {
              setRequest(true);
              animation.start();
              handleSubmit();
            }}
          >
            <ButtonText>BUSCAR NOVAMENTE</ButtonText>
          </Button>
        </BoxEmpty>
      </Box>
    );
  };

  return (
    <SafeBackground>
      <Container>
        <HeaderScreen title="Buscar partida" />
        {request ? <BeforeRequest /> : <AfterRequest />}
      </Container>
    </SafeBackground>
  );
}
