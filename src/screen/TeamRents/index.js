import React, { useState, useEffect } from 'react';
import { useNavigation, useRoute } from '@react-navigation/native';
import { getRentsTeam } from '~/services/endpoints/rents';
import deuFutToast from '~/services/toast';
import HeaderScreen from '~/components/HeaderScreens';
import SafeBackground from '~/components/SafeBackground';
import BoxLoading from '~/components/BoxLoading';
import BoxEmpty from '~/components/BoxEmpty';
import {
  Container,
  BoxDetails,
  CategoryText,
  DetailsText,
  WrapperDetails,
  Wrapper,
} from './styles';

export default function TeamRents() {
  const route = useRoute();
  const navigation = useNavigation();
  const { team_id, player_role } = route.params;
  const [rents, setRents] = useState();
  const [total, setTotal] = useState(0);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getRents();
  }, []);

  const getRents = () => {
    setTimeout(async () => {
      const { apiCall } = getRentsTeam();
      try {
        const response = await apiCall({ team_id });
        setRents(response.data.data);
        setTotal(response.data.records);
        setLoading(false);
      } catch (err) {
        setLoading(false);
        deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
        navigation.goBack();
      }
    }, 1000);
  };

  const removeRent = (rent_id) => {
    const array = [...rents];
    const index = rents.findIndex((rent) => rent.id === rent_id);
    if (index >= 0) {
      array.splice(index, 1);
      setRents(array);
    }
  };

  return (
    <SafeBackground>
      <Container>
        <HeaderScreen title="Aluguéis do time" goBack />
        {loading && <BoxLoading />}
        {!loading && rents.length > 0 && (
          <Wrapper contentContainerStyle={{ paddingBottom: 30 }}>
            {rents.map((rent) => (
              <WrapperDetails
                key={rent.id}
                onPress={() =>
                  navigation.navigate('DetalhesPartidaTime', {
                    rent_id: rent.id,
                    team_id,
                    player_role,
                    removeRent,
                  })
                }
              >
                <BoxDetails>
                  <CategoryText>Campo: </CategoryText>
                  <DetailsText numberOfLines={1}>
                    {rent.footballfield.name}
                  </DetailsText>
                </BoxDetails>
                <BoxDetails>
                  <CategoryText>Data: </CategoryText>
                  <DetailsText numberOfLines={1}>
                    {new Date(rent.date_start).getUTCDate()}/
                    {new Date(rent.date_start).getUTCMonth() + 1}/
                    {new Date(rent.date_start).getUTCFullYear()}
                  </DetailsText>
                </BoxDetails>
                <BoxDetails>
                  <CategoryText>Início: </CategoryText>
                  <DetailsText numberOfLines={1}>
                    {new Date(rent.date_start).getHours()}:
                    {String(new Date(rent.date_start).getMinutes()).padStart(
                      2,
                      '0'
                    )}
                  </DetailsText>
                </BoxDetails>
                <BoxDetails>
                  <CategoryText>Fim: </CategoryText>
                  <DetailsText numberOfLines={1}>
                    {new Date(rent.date_end).getHours()}:
                    {String(new Date(rent.date_end).getMinutes()).padStart(
                      2,
                      '0'
                    )}
                  </DetailsText>
                </BoxDetails>
              </WrapperDetails>
            ))}
          </Wrapper>
        )}
        {!loading && rents.length === 0 && (
          <BoxEmpty text="Nenhum aluguel encontrado!" />
        )}
      </Container>
    </SafeBackground>
  );
}
