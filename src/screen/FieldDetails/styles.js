import styled from 'styled-components/native';
import { colors } from '~/styles';

export const Container = styled.View`
  flex: 1;
  margin-left: 0;
  margin-right: 0;
  background: ${colors.white};
`;

export const Wrapper = styled.ScrollView`
  padding: 10px;
`;

export const WrapperDetails = styled.View`
  position: relative;
  padding: 5px;
  margin: 10px;
  border-width: 1px;
  border-color: ${colors.black};
  border-radius: 10px;
`;

export const BoxDetailsField = styled.View`
  display: flex;
  align-items: center;
  flex-direction: row;
  padding: 5px;
`;

export const CategoryText = styled.Text`
  font-size: 20px;
  font-weight: bold;
  color: #a6b1c3;
`;

export const DetailsText = styled.Text`
  font-size: 18px;
`;

export const WrapperRents = styled.View``;

export const BoxRent = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: 15px 15px;
  margin-bottom: 15px;
  border-width: 1px;
  border-color: grey;
  border-radius: 10px;
`;

export const TextContainer = styled.View`
  flex-direction: row;
  margin-top: 2.5px;
  margin-bottom: 2.5px;
`;

export const BoxGroup = styled.View``;

export const ButtonNavigateRent = styled.View``;

export const TextDetails = styled.Text`
  font-size: 14px;
  font-weight: bold;
`;

export const Text = styled.Text.attrs({ numberOfLines: 1 })`
  color: ${colors.black};
  font-size: 14px;
`;

export const TextDivision = styled.Text`
  color: #a6b1c3;
`;

export const TextReserved = styled.Text`
  color: ${colors.danger2};
  font-size: 12px;
  font-style: italic;
  font-weight: bold;
`;
