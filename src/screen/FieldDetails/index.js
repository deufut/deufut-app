import React, { useCallback, useEffect, useState } from 'react';
import { View, RefreshControl } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import { useNavigation, useRoute } from '@react-navigation/native';
import { getFieldById } from '~/services/endpoints/fields';
import deuFutToast from '~/services/toast';
import BoxLoading from '~/components/BoxLoading';
import BoxEmpty from '~/components/BoxEmpty';
import HeaderScreen from '~/components/HeaderScreens';
import SafeBackground from '~/components/SafeBackground';
import {
  Container,
  Wrapper,
  WrapperDetails,
  BoxDetailsField,
  CategoryText,
  DetailsText,
  WrapperRents,
  BoxRent,
  Text,
  TextContainer,
  BoxGroup,
  ButtonNavigateRent,
  TextDetails,
  TextDivision,
  TextReserved,
} from './styles';
import { colors } from '~/styles';

export default function FieldDetails() {
  const route = useRoute();
  const navigation = useNavigation();

  const [loadingRequest, setLoadingRequest] = useState(false);
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [rents, setRents] = useState([]);

  const loadField = useCallback(async () => {
    const { apiCall } = getFieldById();
    try {
      const response = await apiCall({ id: route.params.id });
      setData(response.data);
      setRents(response.data.rents);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
    }
  }, []);

  useEffect(() => {
    loadField();
  }, [loadField]);

  const RenderRents = () => {
    if (rents.length) {
      return (
        <>
          {rents.map((rent) => {
            const dateStart = new Date(rent.date_start);
            const dateEnd = new Date(rent.date_end);

            if (!rent.reserved) {
              return (
                <BoxRent
                  key={rent.id}
                  onPress={() =>
                    navigation.navigate('EfetuarReserva', { rent_id: rent.id })
                  }
                >
                  <BoxGroup>
                    <TextContainer>
                      <TextDetails>Data: </TextDetails>
                      <Text>
                        {dateStart.getUTCDate()}/{dateStart.getUTCMonth() + 1}/
                        {dateStart.getUTCFullYear()}
                      </Text>
                    </TextContainer>
                    <TextContainer>
                      <TextDetails>Valor: </TextDetails>
                      <Text>
                        R$ {String(rent.value.toFixed(2)).replace('.', ',')}
                      </Text>
                    </TextContainer>
                  </BoxGroup>
                  <BoxGroup>
                    <TextContainer>
                      <TextDetails>Início: </TextDetails>
                      <Text>
                        {dateStart.getHours()}:
                        {String(dateStart.getMinutes()).padStart(2, '0')}
                      </Text>
                    </TextContainer>
                    <TextContainer>
                      <TextDetails>Término: </TextDetails>
                      <Text>
                        {dateEnd.getHours()}:
                        {String(dateEnd.getMinutes()).padStart(2, '0')}
                      </Text>
                    </TextContainer>
                  </BoxGroup>
                  <ButtonNavigateRent>
                    <FontAwesomeIcon
                      icon={faArrowRight}
                      size={20}
                      color={colors.primary}
                    />
                  </ButtonNavigateRent>
                </BoxRent>
              );
            }
            return (
              <BoxRent key={rent.id} disabled>
                <BoxGroup>
                  <TextContainer>
                    <TextDetails>Data: </TextDetails>
                    <Text>
                      {dateStart.getUTCDate()}/{dateStart.getUTCMonth() + 1}/
                      {dateStart.getUTCFullYear()}
                    </Text>
                  </TextContainer>
                  <TextContainer>
                    <TextDetails>Valor: </TextDetails>
                    <Text>
                      R$ {String(rent.value.toFixed(2)).replace('.', ',')}
                    </Text>
                  </TextContainer>
                </BoxGroup>
                <BoxGroup>
                  <TextContainer>
                    <TextDetails>Inicio: </TextDetails>
                    <Text>
                      {dateStart.getHours()}:
                      {String(dateStart.getMinutes()).padStart(2, '0')}
                    </Text>
                  </TextContainer>
                  <TextContainer>
                    <TextDetails>Término: </TextDetails>
                    <Text>
                      {dateEnd.getHours()}:
                      {String(dateEnd.getMinutes()).padStart(2, '0')}
                    </Text>
                  </TextContainer>
                </BoxGroup>
                <TextReserved>Reservado</TextReserved>
              </BoxRent>
            );
          })}
        </>
      );
    }

    return <BoxEmpty text="Nenhum aluguél encontrado!" height={250} />;
  };

  return (
    <SafeBackground>
      <Container>
        <HeaderScreen title="Detalhes do campo" goBack />
        {loading && <BoxLoading />}
        {!loading && (
          <>
            <WrapperDetails>
              <BoxDetailsField>
                <CategoryText>Nome do time: </CategoryText>
                <DetailsText>{data.name}</DetailsText>
              </BoxDetailsField>
              <BoxDetailsField>
                <CategoryText>Contato: </CategoryText>
                <DetailsText>{data.phone}</DetailsText>
              </BoxDetailsField>
              <BoxDetailsField>
                <CategoryText>Descrição: </CategoryText>
                <DetailsText>{data.description}</DetailsText>
              </BoxDetailsField>
              <BoxDetailsField>
                <CategoryText>Estado: </CategoryText>
                <DetailsText>{data.state}</DetailsText>
              </BoxDetailsField>
              <BoxDetailsField>
                <CategoryText>Cidade: </CategoryText>
                <DetailsText>{data.city}</DetailsText>
              </BoxDetailsField>
              <BoxDetailsField>
                <CategoryText>Endereço: </CategoryText>
                <DetailsText>{data.address}</DetailsText>
              </BoxDetailsField>
              <BoxDetailsField>
                <CategoryText>CEP: </CategoryText>
                <DetailsText>{data.cep}</DetailsText>
              </BoxDetailsField>
              <BoxDetailsField>
                <CategoryText>Bairro: </CategoryText>
                <DetailsText>{data.neighborhood}</DetailsText>
              </BoxDetailsField>
            </WrapperDetails>
            <View
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
                flexDirection: 'row',
                marginTop: 10,
                marginBottom: 10,
              }}
            >
              <View
                style={{
                  height: 2,
                  width: '32%',
                  backgroundColor: '#A6B1C3',
                }}
              />
              <TextDivision>Aluguéis disponíveis</TextDivision>
              <View
                style={{
                  height: 2,
                  width: '32%',
                  backgroundColor: '#A6B1C3',
                }}
              />
            </View>
            <Wrapper
              refreshControl={
                <RefreshControl
                  tintColor="transarent"
                  onRefresh={() => loadField()}
                  refreshing={false}
                />
              }
            >
              <WrapperRents>
                <RenderRents />
              </WrapperRents>
            </Wrapper>
          </>
        )}
      </Container>
    </SafeBackground>
  );
}
