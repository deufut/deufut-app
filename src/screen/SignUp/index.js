import React, { useState, useEffect } from 'react';
import {
  faEnvelope,
  faUser,
  faKey,
  faPhone,
} from '@fortawesome/free-solid-svg-icons';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { signUpRequest, setLoading } from '~/store/modules/auth/actions';
import KeyboardScrollBackground from '~/components/KeyboardScrollBackground';
import ActivityIndicator from '~/components/ActivityIndicator';
import Input from '~/components/Input';
import {
  Content,
  Form,
  Button,
  ButtonText,
  Splash,
  Logo,
  SignInContainer,
  LinkText,
} from './styles';
import { colors } from '~/styles';

export default function SignUp() {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const { loading } = useSelector((state) => state.auth);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');
  const [phone, setPhone] = useState('');

  useEffect(() => {
    dispatch(setLoading(false));
  }, []);

  function handleSubmit() {
    if (
      name &&
      email &&
      password &&
      passwordConfirmation &&
      phone &&
      password === passwordConfirmation
    ) {
      dispatch(signUpRequest(name, email, password, phone));
      navigation.navigate('LogIn');
    }
  }

  return (
    <KeyboardScrollBackground backgroundColor={colors.ice}>
      <Content>
        <Form>
          <Splash>
            <Logo />
          </Splash>
          <Input
            autoCorrect={false}
            autoCapitalize="words"
            value={name}
            onChangeText={(text) => setName(text)}
            placeholder="Nome"
            onSubmitEditing={() => emailInput.focus()}
            blurOnSubmit={false}
            icon={faUser}
          />
          <Input
            ref={(input) => (emailInput = input)}
            keyboardType="email-address"
            autoCorrect={false}
            autoCapitalize="none"
            value={email}
            onChangeText={(text) => setEmail(text)}
            placeholder="E-mail"
            onSubmitEditing={() => passwordInput.focus()}
            blurOnSubmit={false}
            icon={faEnvelope}
          />
          <Input
            ref={(input) => (passwordInput = input)}
            autoCorrect={false}
            autoCapitalize="none"
            value={password}
            onChangeText={(text) => setPassword(text)}
            placeholder="Senha"
            secureTextEntry
            blurOnSubmit={false}
            icon={faKey}
            onSubmitEditing={() => passwordConfirmationInput.focus()}
          />
          <Input
            ref={(input) => (passwordConfirmationInput = input)}
            autoCorrect={false}
            autoCapitalize="none"
            value={passwordConfirmation}
            onChangeText={(text) => setPasswordConfirmation(text)}
            placeholder="Repetir a senha"
            secureTextEntry
            icon={faKey}
            blurOnSubmit={false}
            onSubmitEditing={() => phoneInput.focus()}
          />
          <Input
            ref={(input) => (phoneInput = input)}
            keyboardType="phone-pad"
            autoCorrect={false}
            value={phone}
            onChangeText={(text) => setPhone(text)}
            placeholder="Celular"
            icon={faPhone}
            onSubmitEditing={() => (!loading ? handleSubmit() : null)}
          />
          <Button onPress={() => (!loading ? handleSubmit() : null)}>
            {loading ? (
              <ActivityIndicator color="white" />
            ) : (
              <ButtonText>CADASTRAR</ButtonText>
            )}
          </Button>
          <SignInContainer onPress={() => navigation.navigate('LogIn')}>
            <LinkText>Já possui cadastro?</LinkText>
          </SignInContainer>
        </Form>
      </Content>
    </KeyboardScrollBackground>
  );
}
