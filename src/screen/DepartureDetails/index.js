import React, { useEffect, useState } from 'react';
import { View, Text } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faCrown } from '@fortawesome/free-solid-svg-icons';
import { useNavigation, useRoute } from '@react-navigation/native';
import { getRentById } from '~/services/endpoints/rents';
import deuFutToast from '~/services/toast';
import SafeBackground from '~/components/SafeBackground';
import HeaderScreen from '~/components/HeaderScreens';
import BoxLoading from '~/components/BoxLoading';
import {
  Container,
  Wrapper,
  WrapperDetails,
  BoxDetails,
  CategoryText,
  DetailsText,
  Button,
  ButtonText,
  RolePlayer,
} from './styles';

export default function DepartureDetails() {
  const route = useRoute();
  const navigation = useNavigation();
  const { rent_id } = route.params;
  const [rent, setRent] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getRent();
  }, []);

  const getRent = async () => {
    const { apiCall } = getRentById();
    try {
      setLoading(true);
      const response = await apiCall(rent_id);
      setRent(response.data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
      navigation.goBack();
    }
  };

  return (
    <SafeBackground>
      <Container>
        <HeaderScreen title="Detalhes da partida" goBack />
        {loading && <BoxLoading />}
        {!loading && rent && (
          <Wrapper contentContainerStyle={{ paddingBottom: 40 }}>
            <WrapperDetails>
              <BoxDetails>
                <CategoryText>Campo: </CategoryText>
                <DetailsText numberOfLines={1}>
                  {rent.footballfield.name}
                </DetailsText>
              </BoxDetails>
              <BoxDetails>
                <CategoryText>Endereço: </CategoryText>
                <DetailsText>{rent.footballfield.address}</DetailsText>
              </BoxDetails>
              <BoxDetails>
                <CategoryText>Número: </CategoryText>
                <DetailsText>{rent.footballfield.number}</DetailsText>
              </BoxDetails>
              <BoxDetails>
                <CategoryText>Bairro: </CategoryText>
                <DetailsText>{rent.footballfield.neighborhood}</DetailsText>
              </BoxDetails>
              <BoxDetails>
                <CategoryText>Cidade: </CategoryText>
                <DetailsText>{rent.footballfield.city}</DetailsText>
              </BoxDetails>
              <BoxDetails>
                <CategoryText>Estado: </CategoryText>
                <DetailsText>{rent.footballfield.state}</DetailsText>
              </BoxDetails>
              <BoxDetails>
                <CategoryText>Data: </CategoryText>
                <DetailsText numberOfLines={1}>
                  {new Date(rent.date_start).getUTCDate()}/
                  {new Date(rent.date_start).getUTCMonth() + 1}/
                  {new Date(rent.date_start).getUTCFullYear()}
                </DetailsText>
              </BoxDetails>
              <BoxDetails>
                <CategoryText>Início: </CategoryText>
                <DetailsText numberOfLines={1}>
                  {new Date(rent.date_start).getHours()}:
                  {String(new Date(rent.date_start).getMinutes()).padStart(
                    2,
                    '0'
                  )}
                </DetailsText>
              </BoxDetails>
              <BoxDetails>
                <CategoryText>Fim: </CategoryText>
                <DetailsText numberOfLines={1}>
                  {new Date(rent.date_end).getHours()}:
                  {String(new Date(rent.date_end).getMinutes()).padStart(
                    2,
                    '0'
                  )}
                </DetailsText>
              </BoxDetails>
              <BoxDetails>
                <CategoryText>Valor: </CategoryText>
                <DetailsText numberOfLines={1}>
                  R$ {String(rent.value.toFixed(2)).replace('.', ',')}
                </DetailsText>
              </BoxDetails>
            </WrapperDetails>
            <View
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
                flexDirection: 'row',
                marginTop: 10,
                marginBottom: 10,
              }}
            >
              <View
                style={{
                  height: 2,
                  width: '30%',
                  backgroundColor: '#A6B1C3',
                }}
              />
              <Text style={{ color: '#A6B1C3' }}>Times participantes</Text>
              <View
                style={{
                  height: 2,
                  width: '30%',
                  backgroundColor: '#A6B1C3',
                }}
              />
            </View>
            {rent.teams.map((team) => (
              <WrapperDetails key={team.id}>
                <BoxDetails
                  key={team.id}
                  style={{ justifyContent: 'space-between' }}
                >
                  <View style={{ flexDirection: 'row' }}>
                    <CategoryText>Time: </CategoryText>
                    <DetailsText numberOfLines={1}>{team.name}</DetailsText>
                  </View>
                  {team.role === 'Dono' && (
                    <FontAwesomeIcon icon={faCrown} size={28} color="#FFAA2B" />
                  )}
                  {team.role !== 'Dono' && (
                    <RolePlayer rolePlayer={team.role}>{team.role}</RolePlayer>
                  )}
                </BoxDetails>
              </WrapperDetails>
            ))}
            <Button
              onPress={() =>
                navigation.navigate('EfetuarReserva', {
                  rent_id: rent.id,
                  update: () => getRent(),
                })
              }
            >
              <ButtonText>PARTICIPAR</ButtonText>
            </Button>
          </Wrapper>
        )}
      </Container>
    </SafeBackground>
  );
}
