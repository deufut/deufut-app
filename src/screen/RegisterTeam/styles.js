import styled from 'styled-components/native';
import { colors } from '~/styles';

export const Container = styled.View`
  flex: 1;
  margin-left: 0;
  margin-right: 0;
  background: ${colors.white};
`;

export const Title = styled.Text`
  font-size: 22px;
  font-weight: bold;
  color: ${colors.fontColor};
  margin-top: 13px;
  margin-left: 20px;
  margin-bottom: 5px;
`;

export const Button = styled.TouchableOpacity`
  width: 250px;
  height: 40px;
  border-radius: 5px;
  margin-top: 20px;
  background: ${colors.primary};
  align-self: center;
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text`
  color: ${colors.white};
  font-size: 15px;
  font-weight: bold;
`;

export const Wrapper = styled.ScrollView``;

export const BoxImage = styled.View`
  width: 100%;
  display: flex;
  align-items: center;
  padding: 15px;
`;

export const BoxInput = styled.View`
  width: 100%;
  padding: 0 10px;
`;

export const Input = styled.TextInput`
  margin-left: 15px;
  margin-right: 15px;
  margin-top: 8px;
  padding: 0 10px;
  height: 40px;
  border-color: grey;
  border-width: 1px;
  border-radius: 6px;
`;

export const CategoryTeam = styled.Text`
  margin-left: 15px;
  margin-right: 15px;
  margin-top: 20px;
  font-size: 18px;
`;
