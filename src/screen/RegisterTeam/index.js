import React, { useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import { refreshMyTeamList } from '~/store/modules/utils/actions';
import SafeBackground from '~/components/SafeBackground';
import HeaderScreen from '~/components/HeaderScreens';
import ActivityIndicator from '~/components/ActivityIndicator';
import { registerTeam } from '~/services/endpoints/teams';
import deuFutToast from '~/services/toast';
import {
  Container,
  ButtonText,
  Wrapper,
  BoxInput,
  CategoryTeam,
  Input,
  Button,
  BoxImage,
} from './styles';
import ImageShield from '~/assets/svg/football.svg';

export default function RegisterTeam() {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [state, setState] = useState('');
  const [city, setCity] = useState('');
  const [loading, setLoading] = useState(false);

  async function handleSubmit() {
    if (name && description && state && city) {
      setLoading(true);
      try {
        const { apiCall } = registerTeam();
        await apiCall({ name, description, state, city });
        setLoading(false);
        deuFutToast(
          'success',
          'Tudo certo!',
          'Time foi cadastrado com sucesso'
        );
        dispatch(refreshMyTeamList());
        navigation.navigate('MeusTimes');
      } catch (err) {
        setLoading(false);
        deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
      }
    } else {
      deuFutToast('error', 'Algo deu errado!', 'Algum campo está vazio.');
    }
  }

  return (
    <SafeBackground>
      <Container>
        <HeaderScreen title="Cadastre seu time" goBack />
        <Wrapper>
          <BoxImage>
            <ImageShield width={150} height={150} fill="#333" />
          </BoxImage>
          <BoxInput>
            <CategoryTeam>Nome *</CategoryTeam>
            <Input
              name="name"
              autoCapitalize="none"
              autoCorrect={false}
              value={name}
              onChangeText={setName}
              placeholder="Nome do time"
              blurOnSubmit={false}
              onSubmitEditing={() => descriptionInput.focus()}
            />

            <CategoryTeam>Descrição *</CategoryTeam>
            <Input
              ref={(input) => (descriptionInput = input)}
              name="description"
              autoCapitalize="none"
              autoCorrect={false}
              value={description}
              onChangeText={setDescription}
              placeholder="Descrição do time"
              blurOnSubmit={false}
              onSubmitEditing={() => stateInput.focus()}
            />

            <CategoryTeam>Estado *</CategoryTeam>
            <Input
              ref={(input) => (stateInput = input)}
              name="state"
              autoCapitalize="none"
              autoCorrect={false}
              value={state}
              onChangeText={setState}
              placeholder="Estado do time"
              blurOnSubmit={false}
              onSubmitEditing={() => cityInput.focus()}
            />

            <CategoryTeam>Cidade *</CategoryTeam>
            <Input
              ref={(input) => (cityInput = input)}
              name="city"
              autoCapitalize="none"
              autoCorrect={false}
              value={city}
              onChangeText={setCity}
              placeholder="Cidade do time"
              onSubmitEditing={() => handleSubmit()}
            />
          </BoxInput>
          <Button
            onPress={() => {
              if (!loading) {
                handleSubmit();
              }
            }}
          >
            {!loading ? (
              <ButtonText>CADASTRAR TIME</ButtonText>
            ) : (
              <ActivityIndicator color="white" />
            )}
          </Button>
        </Wrapper>
      </Container>
    </SafeBackground>
  );
}
