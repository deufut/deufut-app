import React, { useState, useEffect } from 'react';
import { useRoute } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import { View } from 'react-native';
import SafeBackground from '~/components/SafeBackground';
import {
  Container,
  ButtonText,
  Wrapper,
  BoxInput,
  CategoryTeam,
  Input,
  Button,
  BoxImage,
} from './styles';
import HeaderScreen from '~/components/HeaderScreens';
import Edit from '~/assets/svg/editTeam.svg';
import { getTeamById, editTeam } from '~/services/endpoints/teams';
import deuFutToast from '~/services/toast';
import {
  refreshTeamDetails,
  refreshMyTeamList,
} from '~/store/modules/utils/actions';
import ActivityIndicator from '~/components/ActivityIndicator';
import BoxLoading from '~/components/BoxLoading';

export default function EditTeam() {
  const route = useRoute();

  const dispatch = useDispatch();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [state, setState] = useState('');
  const [city, setCity] = useState('');
  const [loading, setLoading] = useState(false);
  const [loadingEdit, setLoadingEdit] = useState(false);

  async function loadDataTeam() {
    const { apiCall } = getTeamById();
    setLoading(true);
    try {
      const response = await apiCall({ id: route.params.id });
      const {
        name: nameTeam,
        description: descriptionTeam,
        city: cityTeam,
        state: stateTeam,
      } = response.data;

      setName(nameTeam);
      setDescription(descriptionTeam);
      setCity(cityTeam);
      setState(stateTeam);

      setLoading(false);
      dispatch(refreshMyTeamList());
    } catch (err) {
      setLoading(false);
      deuFutToast(
        'error',
        'Falha',
        'Erro ao carregar dados do time, tente novamente mais tarde...'
      );
    }
  }

  useEffect(() => {
    loadDataTeam();
  }, []);

  async function handleSubmit() {
    if (name && description && state && city) {
      setLoadingEdit(true);
      try {
        const { apiCall } = editTeam();
        await apiCall({ id: route.params.id, name, description, state, city });
        setLoadingEdit(false);
        deuFutToast(
          'success',
          'Tudo certo!',
          'Seu time foi editado com sucesso'
        );
        dispatch(refreshTeamDetails());
      } catch (err) {
        setLoadingEdit(false);
        deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
      }
    } else {
      deuFutToast('error', 'Algo deu errado!', 'Algum campo está vazio.');
    }
  }

  return (
    <SafeBackground>
      <Container>
        <HeaderScreen title="Edite seu time" goBack />
        <Wrapper>
          <BoxImage>
            <Edit width={140} height={140} fill="#333" />
          </BoxImage>
          {loading && (
            <View
              style={{
                marginTop: 120,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <BoxLoading />
            </View>
          )}
          {!loading && (
            <>
              <BoxInput>
                <CategoryTeam>Nome *</CategoryTeam>
                <Input
                  name="name"
                  autoCapitalize="none"
                  autoCorrect={false}
                  value={name}
                  onChangeText={setName}
                  placeholder="Nome do time"
                  editable={!loading}
                  blurOnSubmit={false}
                  onSubmitEditing={() => descriptionInput.focus()}
                />

                <CategoryTeam>Descrição *</CategoryTeam>
                <Input
                  ref={(input) => (descriptionInput = input)}
                  name="description"
                  autoCapitalize="none"
                  autoCorrect={false}
                  value={description}
                  onChangeText={setDescription}
                  placeholder="Descrição do time"
                  editable={!loading}
                  blurOnSubmit={false}
                  onSubmitEditing={() => stateInput.focus()}
                />

                <CategoryTeam>Estado *</CategoryTeam>
                <Input
                  ref={(input) => (stateInput = input)}
                  name="state"
                  autoCapitalize="none"
                  autoCorrect={false}
                  value={state}
                  onChangeText={setState}
                  placeholder="Estado do time"
                  editable={!loading}
                  blurOnSubmit={false}
                  onSubmitEditing={() => cityInput.focus()}
                />

                <CategoryTeam>Cidade *</CategoryTeam>
                <Input
                  ref={(input) => (cityInput = input)}
                  name="city"
                  autoCapitalize="none"
                  autoCorrect={false}
                  value={city}
                  onChangeText={setCity}
                  placeholder="Cidade do time"
                  editable={!loading}
                  onSubmitEditing={() => handleSubmit()}
                />
              </BoxInput>
              <Button onPress={handleSubmit}>
                {!loadingEdit ? (
                  <ButtonText>ALTERAR DADOS</ButtonText>
                ) : (
                  <ActivityIndicator color="white" />
                )}
              </Button>
            </>
          )}
        </Wrapper>
      </Container>
    </SafeBackground>
  );
}
