import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { signOut } from '~/store/modules/auth/actions';
import SafeBackground from '~/components/SafeBackground';
import HeaderScreen from '~/components/HeaderScreens';
import ActivityIndicator from '~/components/ActivityIndicator';
import deuFutToast from '~/services/toast';
import {
  Container,
  Button,
  ButtonText,
  Wrapper,
  Form,
  BoxImage,
  BoxInput,
  Input,
  CategoryProfile,
  EditPasswordButton,
  TextEdit,
} from './styles';
import ImageProfile from '~/assets/svg/user.svg';

import { editUserRequest } from '~/store/modules/users/actions';

export default function Profile() {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const { user, loading } = useSelector((state) => state.users);
  const [name, setName] = useState(user.name ? user.name : '');
  const [email, setEmail] = useState(user.email ? user.email : '');
  const [phone, setPhone] = useState(user.phone ? user.phone : '');

  function handleSubmit() {
    if (name && email && phone) {
      dispatch(
        editUserRequest({
          name,
          phone,
          email,
        })
      );
    } else {
      deuFutToast('error', 'Algo deu errado!', 'Algum campo está vazio.');
    }
  }

  return (
    <SafeBackground>
      <Container>
        <HeaderScreen title="Meus dados" />
        <Wrapper>
          <BoxImage>
            <ImageProfile width={150} height={150} fill="#333" />
          </BoxImage>
          <Form>
            <BoxInput>
              <CategoryProfile>Nome</CategoryProfile>
              <Input
                name="name"
                value={name}
                autoCapitalize="none"
                autoCorrect={false}
                onChangeText={(text) => setName(text)}
                blurOnSubmit={false}
                onSubmitEditing={() => emailInput.focus()}
              />

              <CategoryProfile>E-mail</CategoryProfile>
              <Input
                ref={(input) => (emailInput = input)}
                name="email"
                value={email}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="email-address"
                onChangeText={(text) => setEmail(text)}
                blurOnSubmit={false}
                onSubmitEditing={() => phoneInput.focus()}
              />

              <CategoryProfile>Celular</CategoryProfile>
              <Input
                ref={(input) => (phoneInput = input)}
                name="phone"
                autoCorrect={false}
                autoCapitalize="none"
                value={phone}
                keyboardType="phone-pad"
                onChangeText={(text) => setPhone(text)}
                onSubmitEditing={() => handleSubmit()}
              />

              <EditPasswordButton
                onPress={() => navigation.navigate('AlterarSenha')}
              >
                <TextEdit>Alterar senha</TextEdit>
              </EditPasswordButton>
            </BoxInput>
            <Button
              onPress={() => {
                if (!loading) {
                  handleSubmit();
                }
              }}
            >
              {loading ? (
                <ActivityIndicator color="white" />
              ) : (
                <ButtonText>ALTERAR DADOS</ButtonText>
              )}
            </Button>
            <Button
              style={{ marginTop: 10, backgroundColor: '#F32525' }}
              onPress={() => dispatch(signOut())}
            >
              <ButtonText>SAIR</ButtonText>
            </Button>
          </Form>
        </Wrapper>
      </Container>
    </SafeBackground>
  );
}
