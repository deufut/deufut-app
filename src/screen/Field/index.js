import React, { useEffect, useState } from 'react';
import { RefreshControl, TouchableOpacity } from 'react-native';
import { useSelector } from 'react-redux';
import { faArrowRight, faSearch } from '@fortawesome/free-solid-svg-icons';
import { useNavigation } from '@react-navigation/native';
import { getFields } from '~/services/endpoints/fields';
import deuFutToast from '~/services/toast';
import HeaderScreen from '~/components/HeaderScreens';
import BoxLoading from '~/components/BoxLoading';
import BoxEmpty from '~/components/BoxEmpty';
import SafeBackground from '~/components/SafeBackground';
import {
  Container,
  SubHeader,
  InputWrapper,
  IconSearch,
  Input,
  WrapperFields,
  BoxField,
  BoxRight,
  FieldName,
  DetailsField,
} from './styles';

export default function Field() {
  const navigation = useNavigation();
  const { refreshFieldsList } = useSelector((state) => state.utils);
  const [name, setName] = useState('');
  const [fields, setFields] = useState([]);
  const [loading, setLoading] = useState(true);
  const [recordsTotal, setRecordsTotal] = useState(0);

  async function loadFields() {
    const { apiCall } = getFields();
    try {
      const response = await apiCall({
        start: 0,
        length: 0,
        filters: {
          name,
          state: '',
          city: '',
        },
      });
      const { data, records } = response.data;
      setRecordsTotal(records);
      setFields(data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      deuFutToast('error', 'Algo deu errado!', 'Tente novamente mais tarde.');
    }
  }

  useEffect(() => {
    loadFields();
  }, [refreshFieldsList]);

  return (
    <SafeBackground>
      <Container>
        <HeaderScreen title="Buscar campos" />
        <SubHeader>
          <InputWrapper>
            <Input
              placeholder="Buscar nome do campo"
              ref={(input) => (nameInput = input)}
              autoCorrect={false}
              value={name}
              onChangeText={(text) => setName(text)}
              onSubmitEditing={() => loadFields()}
            />
            <TouchableOpacity onPress={() => loadFields()}>
              <IconSearch size={22} icon={faSearch} />
            </TouchableOpacity>
          </InputWrapper>
        </SubHeader>
        {loading && <BoxLoading />}
        {!loading && (
          <WrapperFields
            refreshControl={
              <RefreshControl
                tintColor="transarent"
                onRefresh={() => loadFields()}
                refreshing={false}
              />
            }
            contentContainerStyle={{ paddingBottom: 20 }}
          >
            {!!fields.length && (
              <>
                {fields.map((field) => (
                  <BoxField
                    key={field.id}
                    onPress={() =>
                      navigation.navigate('DetalhesCampo', { id: field.id })
                    }
                  >
                    <BoxRight>
                      <FieldName>{field.name}</FieldName>
                    </BoxRight>
                    <DetailsField icon={faArrowRight} size={20} />
                  </BoxField>
                ))}
              </>
            )}
            {recordsTotal === 0 && <BoxEmpty text="Nenhum campo encontrado!" />}
          </WrapperFields>
        )}
      </Container>
    </SafeBackground>
  );
}
